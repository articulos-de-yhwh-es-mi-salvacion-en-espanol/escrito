# El pan del cielo - ¿Qué es esto?

En las escrituras se muestra como el pueblo de Israel quería comer carne y se quejaba de que no había alimento y deseaba la carne que comía en Egipto. 

> **Éxodo 16:3** y les decían los hijos de Israel: Ojalá hubiéramos muerto por mano de Jehová en la tierra de Egipto, cuando nos sentábamos a las ollas de carne, cuando comíamos pan hasta saciarnos; pues nos habéis sacado a este desierto para matar de hambre a toda esta multitud.

Esto nos habla de los deseos de la carne que muchas veces aún deseamos saciarnos con los deleites de la carne de nuestra vida anterior siendo ya libres de esa esclavitud.

Dios nos ha libertado y traído a una nueva vida, y nuestros deseos deben cambiar, por esto mismo Dios les envió el pan del cielo. 

> **Éxodo 16:4** Y Jehová dijo a Moisés: He aquí yo os haré llover pan del cielo; y el pueblo saldrá, y recogerá diariamente la porción de un día, para que yo lo pruebe si anda en mi ley, o no.

Dios quiere alimentar nuestro ser con lo que procede de él y no lo que hay acá en la tierra, pero muchas veces cuando leemos la palabra de Dios nos hacemos una pregunta **¿Qué es esto?** lo mismo que dijeron los israelitas en su tiempo:

> **Éxodo 16:15** Y viéndolo los hijos de Israel, se dijeron unos a otros: ¿Qué es esto? porque no sabían qué era. Entonces Moisés les dijo: Es el pan que Jehová os da para comer.

También le pusieron por nombre maná que significa **¿Qué es esto?** 

> **Éxodo 16:31** Y la casa de Israel lo llamó Maná; y era como semilla de culantro, blanco, y su sabor como de hojuelas con miel.

Lo interesante es que el nombre quedo siempre como una pregunta pero la respuesta fue dada hasta la manifestación de Cristo Jesús. 

> **Juan 6:33** Porque el pan de Dios es aquel que descendió del cielo y da vida al mundo.

> **Juan 6:35** Jesús les dijo: Yo soy el pan de vida; el que a mí viene. nunca tendrá hambre; y el que en mí cree, no tendrá hambre; y el que en mí cree, no tendrá sed jamás.

A lo que quiero llegar es que toda pregunta que tengas de las escrituras la respuesta es «Cristo Jesús», el alimento que necesitas para vivir es «Jesucristo», porque él es la vida pero así como el pueblo de Israel se levantaba diariamente a recoger el maná del cielo de la misma manera debemos levantarnos cada mañana e ir a tomar de la misericordia y la palabra de vida que Dios tiene para nosotros cada día.

Dios quiere alimentarnos con la vida de su hijo Jesucristo, por esto debemos orar y leer su palabra porque estaremos comiendo del maná de vida pondrá en nuestra mente y corazón su verdad (Jesucristo), para que nuestro deseo de carne (vieja naturaleza) se vaya. 

Y aunque muchas veces al leer su palabra o vivir cosas en él nos llevara a preguntarnos ¿Qué es esto? entendamos que Dios lo que quiere es revelarnos a su hijo Jesucristo, y su revelación vendrá y reconoceremos que Jesucristo es la vida.

