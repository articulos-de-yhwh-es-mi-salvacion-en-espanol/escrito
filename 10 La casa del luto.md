# La casa del luto

> **Eclesiastés 7:2[^RV60]** Mejor es ir a la casa del luto que a la casa del banquete; porque aquello es el fin de todos los hombres, y el que vive lo pondrá en su corazón.

> **Eclesiasté 7:3[^RV60]** Mejor es el pesar que la risa; porque con la tristeza del rostro se enmendará el corazón.

> **Eclesiastés 7:4[^RV60]** El corazón de los sabios está en la casa del luto; mas el corazón de los insensatos, en la casa en que hay alegría.

El escritor de Eclesiastés da a conocer algo que es muy interesante y a la vez impactante, ya que hace un llamado a ir a la casa del luto, es decir, a un lugar donde podamos lamentar, meditar y saber que hay una muerte. También hace un contraste con la casa de la alegría y la coloca como insensatez.

Cuando el escritor da a conocer esto, lo hace para dar a entender la importancia de la transformación del pensamiento del hombre, y que este pensamiento puede enmendar su camino, no así el que vive en banquete continuo o solo en la alegría, porque no hay un pensamiento que lo lleve a meditar y por consecuencia que sea enmendado su camino.

Por esto mismo en el versículo 3 del capítulo 7 de Eclesiastés dice que es mejor el pesar que la risa, porque la tristeza del rostro enmendará el corazón.

La ventaja que tenemos de tener nuestro corazón en la casa del luto es que podemos experimentar transformaciones, correcciones, razones de prudencia y sabiduría.

> **Salmo 30:11[^RV60]** Has cambiado mi lamento en baile; Desataste mi silicio, y me ceñiste de alegría.

> **Salmos 30:5[^RV60]** Porque un momento será su ira, Pero su favor dura toda la vida. Por la noche durará el lloro, Y a la mañana vendrá la alegría.

> **Salmos 126:6[^RV60]** Irá andando y llorando el que lleva la preciosa semilla; Mas volverá a venir con regocijo, trayendo sus gavillas.

Cuando vemos esto desde la perspectiva de Cristo Jesús vemos estas palabras

> **Juan 16:20-22[^RV60]** De cierto, de cierto os digo, que vosotros lloraréis y lamentaréis, y el mundo se alegrará; pero aunque vosotros estéis tristes, vuestra tristeza se convertirá en gozo. La mujer cuando da a luz, tiene dolor, porque ha llegado su hora; pero después que ha dado a luz un niño, ya no se acuerda de la angustia, por le gozo de que haya nacido un hombre en el mundo. También vosotros ahora tenéis tristeza; pero os volveré a ver, y se gozará vuestro corazón, y nadie os quitará vuestro gozo.

Jesucristo dijo eso porque iba a ser crucificado, indicando que vendría su muerte, y que ellos llorarían y lamentarían, pero que luego de eso iba a ser transformado en gozo.

Indica lo que Jesucristo padeció en el Gólgota cuando fue crucificado, lamentarnos porque un hombre justo dio su vida en un madero y que esto era el fin de todos los hombres. Y que debemos ser de corazón sabio y colocar nuestro corazón en lo que le sucedió a Jesucristo en el calvario, depositar nuestro corazón ahí porque el Gólgota junto con la crucifixión de Cristo revelan la casa del luto. Ya que el Gólgota era un lugar de muerte y ahí fue crucificado Jesucristo al cual debemos velar, es decir, meditar como lo indica el apóstol Pablo, y saber que aunque él padeció y por un momento sabemos que fue por causa de nuestras rebeliones, también sabemos que esto lo convertirá en gozo porque él fue hecho pecado para que nosotros fuésemos justicia de Dios en Él, Aleluya.

Seamos sabios y vayamos al Gólgota y veamos a Jesucristo crucificado y propongamos no pensar alguna otra cosa sino en Jesucristo y este crucificado porque esto es el fin de todos los hombres y nosotros los que vivimos lo pondremos en nuestro corazón, y aunque tristeza pueda llenar nuestras vidas, esto enmendará nuestro corazón además que nuestra tristeza será transformada en gozo y este gozo no nos será quitado.

> **1 Corintios 2:2[^RV60]** Pues me propuse no saber entre vosotros cosa alguna sino a Jesucristo, y a éste crucificado.

> **2 Corintios 5:21[^RV60]** Al que no conoció pecado, por nosotros lo hizo pecado, para que nosotros fuésemos hechos justicia de Dios en él.

> **Isaías 53:5[^RV60]** Mas él herido fue por nuestras rebeliones, molido por nuestros pecados; el castigo de nuestra paz fue sobre él, y por su llaga fuimos nosotros curados.

> **Juan 19:17[^RV60]** Y él, cargando su cruz, salió al lugar llamado de la Calavera, y en hebreo, Gólgota;

[^RV60]: Biblia Reina Valera 1960

