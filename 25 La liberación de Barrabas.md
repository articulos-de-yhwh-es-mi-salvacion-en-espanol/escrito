# La liberación de Barrabas

> **Mateo 27:26** Entonces les **soltó** a **Barrabás**; y habiendo azotado a Jesús, le entregó para ser crucificado.

En Mateo 27:26 se habla cuando Jesús toma el castigo de la cruz y Barrabás es soltado (liberado), lo interesante de esto, es que Barrabás significa hijo del padre (o maestro).[^G912] [^Barrabas]

Cuan impresionante es la figura que vemos ya que Barrabás era un ladrón y un homicida.

> **Juan 18:40** Entonces todos dieron voces de nuevo, diciendo: No a éste, sino a Barrabás. Y **Barrabás era ladrón.**

> **Marcos 15:7** Y había uno que se llamaba Barrabás, preso con sus compañeros de motín que **habían cometido homicidio en un revuelta**.

> **Juan 10:10** El ladrón no viene sino para hurtar y matar y destruir; yo he venido para que tenga vida, y para que la tengan en abundancia.

Es decir, nosotros representamos a Barrabás ladrones y homicidas aunque por naturaleza fuimos hechos para ser hijos del Padre por causa del pecado fuimos destituidos, y nuestra condena era muy clara pero Cristo Jesús nos remplazó para que Él tomara el lugar de nuestro castigo y para que nuestra liberación se hiciera manifiesta y para que la naturaleza que Dios Padre había puesto en nosotros sea manifestara, es decir, ser hijos del Padre y del Maestro, por medio del creer en Jesucristo.

> **Romanos 3:23** por cuanto todos pecaron, y están destituidos de la gloria de Dios,
>
> **Isaías 53:5** Mas él herido fue por nuestras rebeliones, molido por nuestros pecados; el castigo de nuestra paz fue sobre él, y por su llaga fuimos nosotros curados.

> **2 Corintios 5:21** Al que no conoció pecado, por nosotros lo hizo pecado, para que nosotros fuésemos hechos justicia de Dios en él.

> **1 Pedro 3:18** Porque también Cristo padeció una sola vez por los pecados, el justo por los injustos, para llevarnos a Dios, siendo a la verdad muerto en la carne, pero vivificado en espíritu;

> **Juan 1:12-13** 12 Mas a todos los que le recibieron, a los que creen en su nombre, les dio potestad de ser hechos hijos de Dios; 13 los cuales no son engendrados de sangre, ni de voluntad de carne, ni de voluntad de varón, sino de Dios.

Así que comprendamos que estábamos condenados, pero que se presentó Cristo Jesús, el justo, y tomo nuestro lugar para que seamos libres y que nuestra naturaleza sea como la de Jesús, ser hijos del Padre.

Comprendamos que nuestra maldad era como la de Barrabas, rebeldes, ladrones y homicidas. Pero gracias a Jesús que se dio por nosotros y nos ha rescatado de esa naturaleza para ser mansos y humildes de corazón.

Gloria al Rey, porque el amado Jesús se dio para nuestra liberación y nos ha dado de su espíritu para poder declarar a Dios como nuestro Padre.

> **Romanos 8:15** Pues no habéis recibido el espíritu de esclavitud para estar otra vez en temor, sino que habéis recibido el espíritu de adopción, por el cual clamamos: !!Abba, Padre!

> **Gálatas 4:6** Y por cuanto sois hijos, Dios envió a vuestros corazones el Espíritu de su Hijo, el cual clama: !!Abba, Padre!

[^G912]: **Βαραββᾶς** - _Barabás_ - de origen caldeo [H1347 y G5]; hijo de Aba; Barrabás, un israelita: _Barrabás_
[^Barrabas]: Barrabás es Bar Abbâ ( בר-אבא) y significa ‘hijo del padre’