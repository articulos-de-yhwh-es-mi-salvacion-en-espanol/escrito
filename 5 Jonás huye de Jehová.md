# Jonás huye de Jehová

> **Jonás 1:1-3** **1** Vino palabra de Jehová a Jonás hijo de Amitai, diciendo:  **2** Levántate y ve a Nínive, aquella gran ciudad, y pregona contra ella; porque ha subido su maldad delante de mí. **3** Y Jonás se levantó para huir de la presencia de Jehová a Tarsis, y descendió a Jope, y halló una nave que partía para Tarsis; y pagando su pasaje, entró en ella para irse con ellos a Tarsis, lejos de la presencia de Jehová.
>

Jonás un profeta de Dios cuyo nombre significa *paloma* y cuyo padre es la *verdad* porque Amitai significa *verdad*, es decir, que Jonás es una paloma de la verdad, siendo una paloma sinónimo de un mensajero vemos que Jonás era un mensajero de la verdad, por esto Jehová se acerca a Jonás sabiendo que él lo engendró, le dio su identidad y su función, nacido de la verdad y enviado a ser un mensajero.

Una gloria de humildad y verdad, un representante del Espíritu Santo, llevado a pregonar el evangelio a una ciudad temible como Nínive, ciudad que había sojuzgado al pueblo de Dios.

Por lo que al oír esto se niega a cumplir la voluntad de Dios aunque eso lo lleve a dejar la presencia de Dios que en el original hebreo presencia significa literalmente cara o rostro, la falta de perdón que él tenía o tomar una posición de juez lo llevo a invalidar una orden y negar su identidad de mensajero de la verdad, lanzándose al orgullo y abandonando la humildad, características de su esencia.

Porque Jonás conocía a Dios y sabía que al ir a Nínive y pregonar el mensaje, Dios en un acto de clemencia, piedad y misericordia, se retractaría de su ira y se arrepentiría si Nínive escuchaba el mensaje de destrucción contra ellos por causa de su maldad, al saber esto huye porque quiere ver a sus enemigos caer y no ver una gloria sobre ellos, una gloria llamada arrepentimiento y una bondad sobre un pueblo plenamente cruel e indiferente ante la gloria de un Dios Vivo.

Lo que nos deja ver esto es que podemos conocer a Dios y su verdad, pero la falta de perdón nos llevará a desobedecerle porque su amor va a ser derramado sobre nuestro enemigo, pero en nosotros está ver la ruina de nuestro enemigo.

Cuando en la esencia del evangelio está el perdón hacia nuestros enemigos lo cual es el ministerio de la piedad manifestado por Jesucristo al perdonarnos a nosotros, un mundo que no sabe diferenciar entre su mano izquierda y derecha, un mundo cruel y despiadado, carente de amor, que Dios en su amor, envió a un mensajero de la verdad, en su esencia pura a Jesucristo que a diferencia de Jonás, él dijo:

> **Salmos 40:7** Entonces dije: He aquí, vengo: En el rollo del libro está escrito de mí;

Jehová que dio a su hijo Jesucristo para perdonar todos los pecados de un mundo cruel, y no solo eso sino cambiar la naturaleza de todo aquel que cree en el mensaje de salvación dándole una nacionalidad, una vida nueva, una paternidad y saborear las bondades del amor de Dios y el conocimiento de su diestra.

Hermanos amados busquemos el perdón hacía nuestros enemigos para no huir del llamado de Dios sino poder pregonar el evangelio como Cristo Jesús y así traer más hijos para su gloria.

> **Juan 3:16** Porque de tal manera amó Dios al mundo, que ha dado a su Hijo unigénito, para que todo aquel que en él cree, no se pierda, mas tenga vida eterna.