# No temáis porque buscáis a Jesús

> **Mateo 28:5[^RV60]** Mas el ángel, respondiendo, dijo a las mujeres: No temáis vosotras; porque yo sé que buscáis a Jesús, el que fue crucificado.

Hay algo muy hermoso que le paso a las mujeres que fueron a buscar a Jesús al sepulcro y es que vieron a un ángel de Dios que les dijo: «No temáis vosotras; porque yo sé que buscáis a Jesús, el que fue crucificado».

Si buscamos a Jesucristo no debemos temer de lo que puede acontecer si lo buscamos ya que él nos dará su mensaje «No temáis».

Pero debemos entender que al Jesús que buscamos fue crucificado y resucitó al tercer día.

Por esta razón el ángel les recalca «el que fue crucificado» y en el siguiente versículo les recuerda que resucitó:

>**Mateo 28:6-7[^RV60]** No está aquí, pues ha resucitado, como dijo: Venid, ved el lugar donde fue puesto el Señor. E id pronto y decid a sus discípulos que ha resucitado de los muertos y he aquí va delante de vosotros a Galilea; allí le veréis. He aquí, os lo he dicho.

Cuando busquemos a Jesucristo, recordemos que él fue crucificado por nosotros y que resucitó de entre los muertos, y que a aquellas personas que son sus discípulos debemos recordarles que resucitó, aunque ellos ya lo hayan leído en la escritura, y también recordarles que pronto le veremos.

¡Maranatha! No temamos porque buscamos a Jesucristo, el que fue crucificado.

> **1 Tesalonicenses 4:14-18[^RV60]** **14** Porque si creemos que Jesús murió y resucitó, así también traerá Dios con Jesús a los que durmieron en él. **15** Por lo cual os decimos esto en palabra del Señor: que nosotros que vivimos, que habremos quedado hasta la venida del Señor, no precederemos a los que durmieron. **16** Porque el Señor mismo con voz de mando, con voz de arcángel, y con trompeta de Dios, descenderá del cielo; y los muertos en Cristo resucitarán primero. **17** Luego nosotros los que vivimos, los que hayamos quedado, seremos arrebatados juntamente con ellos en las nubes para recibir al Señor en el aire, y así estaremos siempre con el Señor. **18** Por tanto, alentaos los unos a los otros con estas palabras.

[^RV60]: Biblia Reina Valera 1960