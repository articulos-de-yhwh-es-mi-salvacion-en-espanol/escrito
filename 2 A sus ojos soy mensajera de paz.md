# A sus ojos soy mensajera de paz

> **Cantares 8:10[^BNP]**  Yo soy una muralla, y mis pechos como torres; pero a sus ojos soy mensajera de paz.

En el libro de Cantar de los Cantares se da el caso que se describen características del amado y de la amada, es decir, de Cristo y de su amada, pero en el caso de *Cantares 8:10* la amada describe dos cosas de sí misma, que ella es como una muralla y que sus pechos son torres, podríamos entender dos características físicas que denotan hermosura, pero luego agrega «pero a sus ojos soy mensajera de paz».

Con esto vemos que Dios no ve la hermosura física de una persona sino que para Él lo hermoso es que «seamos mensajeros de paz». 

Y con esto Cristo nos da a entender que su Amada será hermosa cuando sea mensajera de paz.

> **Isaías 52:7[^RVR1960]**
>  !!Cuán hermosos son sobre los montes los pies del que trae alegres nuevas, del que anuncia la paz, del que trae nuevas del bien, del que publica salvación, del que dice a Sion: !!Tu Dios reina!

> **Romanos 10:15[^RVR1960]**
> ¿Y cómo predicarán si no fueren enviados? Como está escrito: !!Cuán hermosos son los pies de los que anuncian la paz, de los que anuncian buenas nuevas!

[^BNP]: Biblia de Nuestro Pueblo
[^RVR1960]: Biblia Reina-Valera 1960