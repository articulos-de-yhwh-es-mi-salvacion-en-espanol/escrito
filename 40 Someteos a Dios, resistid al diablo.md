# Someteos a Dios, resistid al diablo

> **Santiago 4:7-8** Someteos, pues, a Dios; resistid al diablo, y huirá de vosotros. Acercaos a Dios, y él se acercará a vosotros. Pecadores, limpiad las manos; y vosotros los de doble ánimo, purificad vuestros corazones.

1. Someter: Es subordinarse a alguien u obedecer.
2. Resistir: Es pararse en contra u oponerse.
3. Diablo: Es un calumniador o acusador.

Santiago nos da una gran enseñanza sobre la manera de hacer huir al diablo y que hacer para que Dios se acerque a nosotros.

Y nos dice «Sométanse a Dios», es decir, sean obedientes, sean subordinados de Dios. Regularmente cuando oramos no nos subordinamos a la palabra o presencia de Dios, sino que con palabras persuasivas buscamos darle ordenes a Dios. Oremos en la obediencia de la palabra de Dios.

Busquemos ser obedientes, subordinados a la palabra de Dios, y por medio del sometimiento y establecernos en esta dimensión el diablo huirá de nosotros.

Un ejemplo maravilloso de esto fue cuando Jesucristo fue tentado, Jesucristo se sometió a la palabra y resistió en la obediencia y por consecuencia el diablo huyó de él.

Por otro lado, Dios no solo quiere que el diablo huya de nosotros sino que nosotros nos acerquemos a Él (Dios) y como consecuencia Él (Dios) se acercará a nosotros.

Así que hermanos por amor del que nos llamó de las tinieblas a su luz admirable, sometámonos a Dios, resistamos al diablo y acerquémonos a Dios y él se acercará a nosotros.