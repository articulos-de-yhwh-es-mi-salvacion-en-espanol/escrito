# No conformarse a este siglo, pero conformarse a Cristo

> **Romanos 12:2** No os conforméis a este siglo, sino transformaos por medio de la renovación de vuestro entendimiento, para que comprobéis cuál es la buena voluntad de Dios, agradable y perfecta.

El Espíritu Santo por medio del apóstol Pablo nos dice que no nos conformemos a este siglo pero cuando vemos que significa conformarse en el texto original para entender un poco más sobre esto nos dice lo siguiente:

***susjematizo***[^G4964] que significa *conformarse al mismo patrón* pero que emplea dos palabras que son **sun**[^G4862] y **sjéma**[^G4976]. La primera que es ***sun***[^G4862] es una palabra que denota *unión*, es decir, *estar asociado, en compañía, proceso, parecido, posesión, etc.* Y la segunda, ***sjéma***[^G4976], significa *figura*.

Que nos dice esto que 
1. No debemos estar más unidos a este siglo (sistema de vida del mundo)
2. Que no debemos dejarnos acompañar por el mundo
3. Que no debemos estar asociados con este siglo
4. Que no tengamos el mismo proceso con este mundo
5. Que no tengamos el mismo parecido que este mundo
6. Que no seamos poseídos por este mundo o no nos dejemos poseer por este mundo

Y al hacer lo anterior por consecuencia no tendremos **sjéma** es decir la *figura* de este mundo.

Porque al tener la figura, ser poseídos o tener el mismo proceso del mundo esto nos llevará a tener el mismo juicio que el mundo y su sistema nos llevara a adoptar una figura que nos es la de Jesucristo y por consecuencia si no es la de Cristo Jesús solo adoptamos una figura anticristiana es decir de Anticristo.

Por esto el Espíritu Santo llama a que no nos conformemos a este siglo para no adoptar la figura de anticristos sino que debemos de tomar la figura de Jesucristo el Amado.

Es decir, al no conformarnos a este siglo debemos conformarnos a Cristo Jesús es decir:
1. Estar unidos a Cristo Jesús
2. Debemos dejarnos acompañar por Cristo Jesús
3. Debemos estar asociados con Cristo Jesús
4. Tener el mismo proceso que Cristo Jesús
5. Tener el mismo parecido que Cristo Jesús
6. Ser poseídos por Cristo Jesús y dejarnos poseer por Cristo Jesús.

Como consecuencia tendremos la figura de Cristo Jesús y para esto debemos renovar nuestro entendimiento y comprobaremos su voluntad la cual es buena agradable y perfecta.

[^G4964]: G4964 - **συσχηματίζω** - *susjematízo* - de G4862 y un derivado de G4976; formar en forma parecida, es decir, conformar al mismo patrón (figurativamente): *conformar*.
[^G4862]: G4862 - **σύν** - *sun* - preposición primaria que denota unión; con o junto a (pero mucho más estrecha que G3326 o G3844), es decir, por asociación, compañía, proceso, parecido, posesión, instrumentalidad, adición, etc.: *con. [En composición tiene aplicaciones similares, incluso calidad de completo.]*
[^G4976]: G4976 - **σχῆμα** - *sjéma* - del alternado de G2192; figura (como modo o circunstancia), es decir, (por implicación) externo, condición: *apariencia, condición*