# Te amo, Oh Jehová, fortaleza mía

> **Salmo 18:1** Te amo, oh Jehová, fortaleza mía.

¿En que plenitud pudo haber dicho David estas palabras, «Te amo, oh Jehová, fortaleza mía»? Estas palabras no fueron en un sentido de «Te amo» como muchas veces nosotros las decimos a personas que queremos o amamos, sino que en este caso fue en una plenitud de gloria ya que al leer la descripción del Salmo dice lo siguiente:

> Al músico principal. Salmo de David, siervo de Jehová, el cual dirigió a Jehová las palabras de este cántico el día que le libró Jehová de mano de todos su enemigos, y de mano de Saúl, Entonces dijo:

Al leer lo anterior nos damos cuenta de que la fidelidad de Dios estuvo con David todo el tiempo desde su infancia hasta el establecimiento de su reinado y sobre todas las cosas: persecuciones, difamaciones, ataques, guerras, menosprecios, traiciones, muertes etc, Dios estuvo con David, nunca lo abandono, sino que lo libro de todos sus enemigos, incluso de aquel que lo acogió en su reino (Saúl), como no habría de expresar esas palabras con gran deleite y que estas palabras sean el inicio del Salmo 18.

Esto nos da la esperanza de saber que aunque tengamos dificultades, traiciones, persecuciones, difamaciones, ataques, menosprecios, etc, sobre todo esto al final Dios vencerá a nuestros enemigos y diremos a plenitud y con gran deleite «Te amo, oh Jehová, fortaleza mía», porque el nos librará de todo, entonces al decir en este tiempo «Te amo, Oh Jehová, fortaleza mía» profetizaremos sobre nuestra vida la fidelidad y armonía de Dios en nuestra vida, recordando que así como salvó a David y lo llevo hasta esa expresión de gozo y victoria así mismo lo hará con nosotros porque somos sus hijos.

