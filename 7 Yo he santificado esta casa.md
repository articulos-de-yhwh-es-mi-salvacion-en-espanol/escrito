# Yo he santificado esta casa

> **1 Reyes 9:3** Y le dijo Jehová: Yo he oído tu oración y tu ruego que has hecho en mi presencia. Yo he santificado esta casa que tú has edificado, para poner mi nombre en ella para siempre; y en ella estarán mis ojos y mi corazón todos los días.



En primera de Reyes, capítulo 9, versículo 3, Salomón recibe una respuesta a la oración que hizo a Jehová por la edificación del templo, ahora ¿qué representa esto? Que Salomón es figura de Jesucristo resucitado y que nosotros somos ese templo que él edifica:



> **1 Corintios 3:16** ¿No sabéis que sois templo de Dios, y que el Espíritu de Dios mora en vosotros?



En primera de Corintios capítulo 3 versículo 16 nos indica que somos templo de Dios por lo tanto alguien nos edificó, quien nos edificó se llama Jesucristo por medio del creer en Él.

Cuando Jesucristo viene a nuestras vidas, nos presenta delante de Dios como un templo e intercede por nosotros para que seamos oídos delante de Dios y que su presencia este en nosotros, cuando Jesucristo hace esto el Padre le contesta conforme a su ruego y oración. [^1]



> **Romanos 8:34** ¿Quién es el que condenará? Cristo es el que murió; más aun, el que también resucitó, el que además está a la diestra de Dios, el que también intercede por nosotros.



Lo que el Padre le contesta acerca de nosotros es que el nos santifica para poner su nombre en nosotros, y para que estén sus ojos y su corazón en nosotros todos los días.



>  **1 Reyes 9:3** Y le dijo Jehová: Yo he oído tu oración y tu ruego que has hecho en mi presencia. Yo he santificado esta casa que tú has edificado, para poner mi nombre en ella para siempre; y en ella estarán mis ojos y mi corazón todos los días.

> **1 Corintios 6:11** Y esto erais algunos; mas ya habéis sido lavados, ya habéis sido santificados, ya habéis sido justificados en el nombre del Señor Jesús, y por el Espíritu de nuestro Dios. 

[^1]: 1 Reyes 8:28-55