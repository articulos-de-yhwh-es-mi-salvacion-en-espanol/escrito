# Como shofar

Debemos darnos cuenta de que no solo hay pecado en aquellos que no creen en Cristo, también hay pecado en las personas que están en la nación a la cual Dios nos ha llamado, y debemos dejarnos preparar para poder ser como shofares que denuncien a gran voz la transgresión y el pecado de nuestra nación.

Los shofares son cuernos curvos que eran utilizados para diferentes propósitos como por ejemplo dar una alarma[^alarma], celebración[^celebracion] o llamar a reunión[^reunion], pero era un sonido fuerte y que normalmente es traducido en la Biblia como trompeta.

Además, estos cuernos pertenecían a antílopes y debían ser tratados para poder ser utilizados.

Dios al indicarle a Isaías que alzará su voz como shofar, le indicaba que ya había sido preparado y formado para ejercer el mandato que le fue hecho en Isaías 58:1.

> **Isaías 58:1[^VIN2011]** ¡Clama a todo pulmón, sin restricción; alza tu voz como shofar! Denúnciale a mi pueblo su transgresión, a la casa de Yaaqov su pecado.

Una de las bendiciones que nos ha otorgado el Dios Vivo es la predicación de su evangelio para que todo aquel que crea en Jesucristo, el cual resucito de entre los muertos por el poder de Dios Padre, sea salvado.

Pero en este caso Jehová Dios habla a Isaías diciéndole que clame a todo pulmón; que lo haga como lo haría un shofar.

Es decir, que este dispuesto a ser usado por Él y que serán puestas palabras en su boca, únicamente tiene que tomar la posición de un shofar. Pero que clame no solo a los gentiles o naciones que no conocen el nombre de Cristo Jesús sino también a aquellos que ya le conocen pero que han transgredido contra Dios y además han pecado como familia.

Lo que le dan a Isaías es una voz de alerta para el pueblo, que suene a gran voz, que proclame la transgresión y el pecado del pueblo de Dios, que lo que hacen no agrada a Dios (aunque se piense que se está agradando) sino que lo hacen bajo su propia doctrina, bajo su propio beneficio egoísta.

> **Isaías 58:2** Ellos me consultan cada día, deseosos de aprender mis caminos. Como una nación que hace lo correcto, que no ha abandonado las leyes de su Elohim, me preguntan por el camino recto, están ansiosos de acercarse a Elohim.

En Isaías 58:2 se hace referencia a toda una nación, es decir, a todos aquellos que creen en Cristo Jesús y que quieren buscar de Él pero realmente están en caminos torcidos deseosos de su propio mal aunque ante poniendo el nombre de Jehová. El pueblo de Dios está desviado, torcido en gran manera, además de cegado, pensando que hacen bien pero su corazón está totalmente desviado del propósito de Dios.

A esto somos llamados, a darnos cuenta de que toda la nación de Dios, se ha desviado, ha apartado su corazón de Dios y que es necesario que se clame a todo pulmón, a tomar la forma de un shofar, a dar una voz de alarma, que toda la nación del Dios Vivo es transgresora aunque piense que no lo es, que es pecadora aunque se crea una gran familia y que si volvemos nuestros corazones hacia el Amado de las naciones, él rescatará y restaurará nuestra nación para alabanza de la gloria de su gracia.

Así que alza tu voz como shofar y pregona la transgresión y el pecado del pueblo de Dios.

[^alarma]: **Levítico 25:9** Entonces harás tocar fuertemente la trompeta en el mes séptimo a los diez días del mes; el día de la expiación haréis tocar la trompeta por toda vuestra tierra.
[^celebracion]: **2 Samuel 6:15** Así David y toda la casa de Israel conducían el arca de Jehová con júbilo y sonido de trompeta.
[^reunion]: **Nehemías 4:20** En el lugar donde oyereis el sonido de la trompeta, reuníos allí con nosotros; nuestro Dios peleará por nosotros.
[^VIN2011]: Biblia Versión Israelita Nazarena