# No digas «Soy un niño»

> **Jeremias 1:6-7** Y yo dije: ¡Ah! ¡ah, Señor Jehová! He aquí, no sé hablar, porque soy niño. Y me dijo Jehová: No digas: Soy un niño; porque a todo lo que te envíe irás tú, y dirás todo lo que te mande.



En Jeremías 1:4-5 Dios hace un llamado a Jeremías pero Jeremías exclama que es un niño. Por lo que Dios le dice que no se llame a sí mismo niño.

Jeremías se menospreció así mismo y no vio como Dios lo veía, ni reconoció quien era el que lo estaba llamando. Pero eso mismo Dios le remarca «No digas: Soy un niño» porque Dios no lo veía así, Dios lo veía como un varón, y luego Dios encomienda que se sujete a todo lo que él le ordene, es decir, Dios marca su preeminencia en esto.

Pero si observamos lo que significa la palabra niño vemos que viene de la palabra ***נַעַר náar***[^H5288] que tiene varios significados entre estos tenemos: **criado, muchacho, joven, niño, sirviente**. Con esto vemos que él no solo se menospreció por su edad (juventud), sino también por su inmadurez (niño), estatus social (sirviente) e inexperiencia (muchacho).

Pero Dios no tomo en cuenta eso, Dios solo tomo en cuenta lo siguiente:

> **Jeremías 1:5** Antes que te formase en el vientre te conocí, y antes que nacieses te santifiqué, te dí por profeta a las naciones.

A Dios no le importó que Jeremías tuviese un concepto de sí mismo como esclavo, inmaduro o inexperto, a Dios lo único que le importaba era el conocimiento que Él tenía acerca de Jeremías.

Que quieren decir entonces las palabras de Dios «No digas: Soy un niño», quiere decir: «Yo sé lo que he hecho en ti, sé lo que he formado en ti, sé que puse mi espíritu en ti, y sé que eres mi profeta, por lo que no te digas niño, porque tienes la capacidad de ir hacia dónde te envíe y tienes la capacidad para que digas lo que te diga que dirás, porque yo sé lo que he hecho en ti, formado en ti y puesto en ti.»

[^H5288]: **נַעַר**, ***náar***, de H5287; (concretamente) muchacho (como activamente), de edad de la infancia hasta la adolescencia; por implicación sirviente; también (por intercambiable de sexo), muchacha (de amplitud similar de edad): *cortesano, criada, -o, hijo, (hombre) joven, muchacho, niño, pastor, pequeño, servidor, siervo, sirviente.*