# Conociendo a un discípulo

> **Juan 13:35** En esto conocerán todos que sois mis discípulos, si tuviereis amor los unos con los otros.

Una de las cosas más preciosas que Jesucristo vino a manifestar fue su amor, no un amor vacío y falso, sino un amor puro y sincero. Siendo Jesús, el Cristo, la plenitud de la manifestación del amor, por ser enviado por El Padre, nos ha dejado algo muy importante e impactante y es un nuevo mandamiento.

Este mandamiento es el de «amarnos los unos a los otros» pero no solo eso sino el de «amarnos los unos a los otros como él nos ama».

> **Juan 13:34-35** **34** Un mandamiento nuevo os doy: Que os améis unos a otros; como yo os he amado, que también os améis unos a otros. **35** En esto conocerán todos que sois mis discípulos, si tuviereis amor los unos con los otros.

Aunque suene como algo «ya conocido» es algo muy preciso lo que dijo, ya que antes no había algo para medir realmente como se debía amar al prójimo, ya que su mandamiento dice «Amaras a tu prójimo como a ti mismo».

> **Mateo 22:39** Y el segundo es semejante: Amarás a tu prójimo como a ti mismo.

Y en este pasaje de Juan 13:34 nos da la medida de como debemos amar a nuestro prójimo.

Además de esto en Juan 13:35 Cristo Jesús dice como conocerán todos que somos sus discípulos, ¡acaso no es esto impresionante! Saber a conciencia de que somos sus discípulos, no solo que alguien eche fuera un demonio o que haga una sanidad o milagro, sino «Que nos amemos los unos a los otro, como él nos amó».

Si alguien no ama a su prójimo lo que conlleva perdonarlo, sufrir por causa de él, no guardar rencor, no envidiarlo, todo esto que habla 1 Corintios 13:4-6, y además que este pueda dar su vida por su prójimo, esto indicará un verdadero amor, y por lo tanto un verdadero discípulo.

> **1 Corintios 13:4-6**  **4** El amor es sufrido, es benigno; el amor no tiene envidia, el amor no es jactancioso, no se envanece; **5** no hace nada indebido, no busca lo suyo, no se irrita, no guarda rencor; **6** no se goza de la injusticia, mas se goza de la verdad.

Busquemos amarnos los unos a los otros para que seamos verdaderamente sus discípulos, no un amor superficial, sino un amor que conlleve dar nuestras vidas primeramente por El amado, y por nuestro prójimo, en esto se conocerá que verdaderamente somos sus discípulos, en que nos amemos unos a otros como Él nos amó.