# El pendón de fe

> **Isaías 11:10** Acontecerá en aquel tiempo que la raíz de Isaí, la cual estará puesta por pendón a los pueblos, será buscada por las gentes; y su habitación será gloriosa.

> **Isaías 11:12** Y levantará pendón a las naciones, y juntará los desterrados de Israel, y reunirá los esparcidos de Judá de los cuatro confines de la tierra.

_Pendón_:
1. Bandera o estandarte, generalmente más largo que ancho, que empleaban como insignia distintiva los regimientos, los batallones y otras agrupaciones militares.
2. Estandarte largo colgado de una asta, generalmente acabado en dos puntas, que se lleva en las procesiones como insignia de una iglesia, una cofradía, etc.

Creo que hay algo que Dios ha querido mostrar a todos los pueblos, y es su pendón, el pendón de fe, el cual es Jesucristo crucificado.

Este pendón (estandarte) es una declaración de victoria y manifestación del batallón al cual pertenecemos. 

Cristo crucificado es nuestro pendón al cual podemos correr para hallar refugio y salvación. Así como Moisés declaró Jehová Nisi, lo cual significa Jehová es mi estandarte luego de haber vencido a Amalec, nosotros podemos declarar que Jesucristo Crucificado es nuestra bandera, al haber vencido por medio de su crucifixión a nuestros enemigos.

> **Éxodo 17:15** Y Moisés edificó un altar, y llamó su nombre Jehová-nisi;

Vivamos con el pendón en nuestra diestra, es decir, confiando que Jesucristo nos ha dado salvación y que pertenecemos a su batallón, y no nos esclavicemos nuevamente a lo malo ni regresemos al batallón de la maldad.

> **1 Corintios 7:22** Por precio fuisteis comprados; no os hagáis esclavos de los hombres.

Alcemos el pendón de fe para la salvación y llamamiento de los pueblos, porque la palabra de Dios dice que el sacrificio expiatorio de Cristo Jesús es la reconciliación y unidad entre Dios y los hombres, y no solo eso sino para la unidad entre los pueblos, porque no hay más judío ni griego, sino todos somos uno en Cristo Jesús.

> **Efesios 2:16** y mediante la cruz reconciliar con Dios a ambos en un solo cuerpo, matando en ella las enemistades.

> **Gálatas 3:28** Ya no hay judío ni griego; no hay esclavo ni libre; no hay varón ni mujer; porque todos vosotros sois uno en Cristo Jesús.
