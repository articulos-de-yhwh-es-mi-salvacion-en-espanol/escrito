# Debemos darlo todo

> **Marcos 12:41-44[^RV60]**
**41** Estando Jesús sentado delante del arca de la ofrenda, miraba cómo el pueblo echaba dinero en el arca; y muchos ricos echaban mucho.
**42** Y vino una viuda pobre, y echó dos blancas, o sea un cuadrante.
**43** Entonces llamando a sus discípulos, les dijo: De cierto os digo que esta viuda pobre echó más que todos los que han echado en el arca;
**44** porque todos han echado de lo que les sobra; pero ésta, de su pobreza echó todo lo que tenía, todo su sustento.

Debemos darle a Dios todo lo que tenemos aunque pensemos que no tengamos mucho que dar, debemos entender que él siempre está observándonos y que no ve lo mucho que podamos dar, sino que ve todo lo que podemos dar.

Por esto la viuda de la cual se habla en este verso fue de gran ejemplo porque ella lo dio todo, nosotros así como ella, debemos darlo todo, aunque pensemos que no tengamos mucho, a Dios lo que le interesa es que nos demos por completo sin importar que lo que tengamos sea mucho o poco.

Lo que Dios desea es que nos demos por completo a él, esto lo podemos ver en Abraham cuando Dios le pide que ofrezca a su hijo en holocausto (ofrenda quemada), su hijo era toda su vida, su virilidad, su promesa, su herencia, pero aun así lo dio todo, porque siendo él un hombre rico, no dio de los muchos corderos que podía tener en su rebaño, sino que dio a su hijo.

Cuando vemos esto nos damos cuenta que Abraham es figura de Dios Padre que dio a su hijo unigénito, Jesucristo, para la expiación de nuestros pecados y así traer vida eterna, El Padre lo dio todo, El hijo se dio por completo. 

Tomemos ejemplo de nuestro Amado y busquemos dar todo lo que tenemos sin menospreciarnos sin pensar si es mucho o poco, sino pensar que lo estamos dando todo.

> **Juan 10:18[^RV60]** Nadie me la quita, sino que yo de mí mismo la pongo. Tengo poder para ponerla, y tengo poder para volverla a tomar. Este mandamiento recibí de mi Padre.

> **2 Corintios 5:21[^RV60]** Al que no conoció pecado, por nosotros lo hizo pecado, para que nosotros fuésemos hechos justicia de Dios en él.

> **Lucas 10:27[^RV60]** Aquél, respondiendo, dijo: Amarás al Señor tu Dios con todo tu corazón, y con toda tu alma, y con todas tus fuerzas, y con toda tu mente; a tu prójimo como a ti mismo.

[^RV60]: Biblia Reina Valera 1960
