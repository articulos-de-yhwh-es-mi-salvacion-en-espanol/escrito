# Las aflicciones del tiempo presente no son comparables

> **Romanos 8:18** Pues tengo por cierto que las aflicciones del tiempo presente no son comparables con la gloria venidera que en nosotros ha de manifestarse.

Vivimos en un tiempo de muchas aflicciones, y dependiendo de las circunstancias en las que nos encontramos podemos percibir mayores aflicciones que otras personas, pensando que nuestras aflicciones no tienen comparación.

Pero hay una persona llamada Cristo Jesús que tuvo la mayor aflicción de todas al ser crucificado y entregado a escándalo y murmuración.

Pero Jesucristo sabiendo que iba a pasar aflicción también sabía que tenía un Dios que era su Padre y que luego de esta aflicción, la cual era momentánea, vendría una manifestación gloriosa de Dios en él, y aunque padeció al ser crucificado, azotado, herido y blasfemado, también fue resucitado por el poder de Dios.

Y no solo resucitado sino manifestado Hijo, con un cuerpo de gloria y llamado a estar a la diestra de Dios, dándole un nombre sobre todo nombre, siendo nombrado Rey de reyes y Señor de señores.

Dios lo exalto a lo más alto porque se sujetó bajo lo que dice Romanos 8:18 que aunque tengamos aflicciones tenemos la esperanza gloriosa de que Dios nuestro Padre nos revelara su gloria y toda esta aflicción se olvidara porque no tendrá comparación con la gloria que en nosotros ha de manifestarse, y esa gloria será nuestro consuelo.

Solo tenemos que esperar por un momento en este tiempo pero bajo la esperanza que su gloria pronto nos ha de ser revelada.

Así que erguíos y levantad vuestras cabezas porque nuestra redención está cerca. Gloria a Dios.

>**Hebreos 12:1-2** Por tanto, nosotros también, teniendo en derredor nuestro tan grande nube de testigos, despojémonos de todo peso y del pecado que nos asedia, y corramos con paciencia la carrera que tenemos por delante, puestos los ojos en Jesús, el autor y consumador de la fe, el cual por el gozo puesto delante de él sufrió la cruz, y menospreciando el oprobio, y se sentó a la diestra del trono de Dios.