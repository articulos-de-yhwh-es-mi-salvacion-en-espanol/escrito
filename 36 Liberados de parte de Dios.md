# Liberados de parte de Dios

>**Hechos 12:6-8** Y cuando Herodes le iba a sacar, aquella misma noche estaba Pedro durmiendo entre dos soldados, sujeto con dos cadenas, y los guardas de la puerta custodiaban la cárcel. Y he aquí que se presentó un ángel del Señor, y una luz resplandeció en la cárcel; y tocando a Pedro en el costado, le despertó, diciendo: Levántate pronto. Y las cadenas se le cayeron de las manos. Le dijo el ángel: Cíñete, y átate las sandalias. y lo hizo así. Y le dijo: Envuélvete en tu manto, y sígueme.

## Una liberación de parte de Dios
En el verso 6, Herodes quiere liberar a Pedro, por consecuencia hubiera habido una exaltación al hombre y no a Dios. Dios para que su gloria fuera manifestada y dar a conocer su libertad, envía a su ángel para liberar a Pedro.

Esto nos enseña que Dios quiere liberarnos pero que el hombre querrá tomar de la gloria que debe ser para Dios. Y el enemigo buscará que se alabe a la misericordia del hombre y no a la de Dios pero Dios demostrará su misericordia para que él sea alabado. Y darnos libertad.

## Dormidos y custodiados
Vemos que Pedro estaba dormido y custodiado. Esto nos da a entender que por muchas o pocas razones estamos dormidos espiritualmente y que nuestros opresores están cerca para que no nos despertemos, y no ser libres.

Pero Dios en su misericordia nos enviará su gloria y visitación (ángel) para liberarnos, despertarnos y prepararnos para seguirle.

## Sígueme
Dios no solo procura nuestro despertar espiritual sino que también nuestra liberación y que todo esto sirva para que le sigamos, que vayamos por su camino y no por el nuestro. Porque Jesús es el camino de verdad que lleva a la vida.