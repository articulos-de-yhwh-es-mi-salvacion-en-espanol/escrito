# Para alejarme de mi santuario

> **Ezequiel 8:6 RV60** Me dijo entonces: Hijo de hombre, ¿no ves lo que éstos hacen, las grandes abominaciones que la casa de Israel hace aquí para alejarme de mi santuario? Pero vuélvete aún, y verás abominaciones mayores.

Dios le dice a Ezequiel *«¿no ves lo que éstos hacen?»*, creo que muchas veces estamos cegados por muchas razones o no vemos en el lugar indicado; y no vemos el pecado que estamos cometiendo o las acciones que hacemos y es cuando Dios llama nuestra atención y nos dice *¿no ves lo que estás haciendo?* Y no solo nos habla para llamar nuestra atención e indicarnos que hay cosas que están mal sino también nos indica la gravedad de las cosas, por esto le dice a Ezequiel *«las grandes abominaciones»*, es decir que hacemos *«grandes abominaciones»* delante de Dios y pensamos que «no es nada malo» que es algo normal que todo el mundo hace sin darnos cuenta de lo siguiente:

> **Lucas 16:15 RV60** Entonces les dijo: Vosotros sois los que os justificáis a vosotros mismo delante de los hombres; mas Dios conoce vuestros corazones; **porque lo que los hombres tienen por sublime, delante de Dios es abominación.**

Luego habla a Ezequiel y le dice *«para alejarme de mi santuario»* es decir que Dios no se quiere apartar de nosotros porque nosotros cuando acogemos la fe de Cristo, nos volvemos templo y morada del Espíritu Santo y Dios llama nuestra atención y nos indica la gravedad de las cosas porque no se quiere alejar de nosotros, quiere que toda su presencia more en nosotros, pero nosotros con *nuestras grandes abominaciones* lo alejamos y luego nos preguntamos *«¿Porque no hay más de Dios en mí?»*.

La palabra **abominación**[^H8441] en hebreo puede traducirse como repugnante, asquerosidad o idolatría, y la palabra **grande**[^H1419] en hebreo proviene de la palabra hebrea que puede ser traducida como «ser orgulloso», es decir que nos enorgullecemos de nuestra idolatría, y ¿qué es idolatría? Según el diccionario RAE:

1. Adoración que se da a los ídolos.
2. Amor excesivo y vehemente a alguien o algo.

Lo que podemos ver es que amamos más lo que hacemos que a Dios, nos enorgullecemos de lo que hemos alcanzado y eso nos lleva a idolatrar, nos olvidamos de Dios e idolatramos lo que hemos logrado y decimos «Gracias a Dios». Acá es cuando Dios nos habla y nos dice *«¿No has visto lo que haces? Que es grande tu abominación delante de mí»*. Y esta falta de adoración a Dios hace que terminemos adorando otras cosas, y alejamos a Dios de nuestra vida. Y Dios nos dice *«me estás alejando de mi santuario»*.

Cuando vemos en Lucas 16:15 la frase *«porque lo que los hombres tienen por sublime, delante de Dios es abominación»* Dios expande nuestro entendimiento para que comprendamos que es una abominación, lo que hizo aquí Jesucristo fue dividir, separó la luz de tinieblas, indicando que lo que los hombres tienen por sublime esto quiere decir lo que los hombres tienen por extraordinariamente bello o excepcional o privilegiado, Dios lo tiene por abominación.

Solo debemos observar lo que hay a nuestro alrededor lo que los hombres han puesto por extraordinariamente bello, excepcional o privilegiado y nos daremos cuenta que es una abominación y darnos cuenta si eso estamos haciendo y apartarnos de esto para dejarlo de adorar. Y adorar a Dios y dejar que su presencia no se aparte de nosotros sino que more plenamente en nosotros.

Pero bendito sea su nombre que nos habla y endereza nuestros caminos para que su presencia plena esté en nuestra vida, derrumbando a todos esos ídolos que hemos levantado o que nos han enseñado a adorar desde nuestra infancia. Solo debemos preguntarle a Dios que es lo que estamos idolatrando y orar para que esos ídolos sean destruidos en nuestra vida y la presencia del Dios Omnipotente more en nosotros. Alabando sea su nombre.

> **Salmos 138:6 RV60** Porque Jehová es excelso, y atiende al humilde, Mas al altivo mira de lejos.

> **1 Corintios 3:16 RV60** ¿No sabéis que sois templo de Dios, y que el Espíritu de Dios mora en vosotros?

[^RV60]: Biblia Reina Valera 1960
[^H8441]: **תּוֹעֵבָה** - *toebá* - o תּוֹעֵבַה toebá; participio activo femenino de H8581; propiamente algo repugnante (moralmente), es decir, (como sustantivo) asquerosidad; especialmente idolatría o (concretamente) ídolo: *abominable, abominación, idolatría, ídolo.*
[^H1431]: **גָּדַל** - *gadal* - raíz primaria torcer [Compárese con H1434], es decir, ser (causativo hacer) grande (en varios sentidos, como en cuerpo, mente, condición u honor, también en orgullo): *alzarse contra, aumentar, crecer, criar, enaltecer, engrandecer, -se, enriquecer, -se, ensoberbecerse, estimar preciosa, exaltar, exceder, gloriosamente, grande, hacer grandes cosas, hacerse grande, hacerse poderoso, jactarse, magnificar, magnífico, mayor, sobremanera, subir de punto, subir el precio.*
[^H1419]: **גָּדוֹל** - *gadol* - o (abreviación) גָּדוֹל gadol; de H1431; grande (en cualquier sentido); de aquí, más viejo; también insolente: *alto, excelente, en extremo, fuerte, gran, grande, jactanciosamente, más grande, muy grande, grandemente, grandeza, grandioso, grave, magnífica, maravilla, mayor, muy, principal, recio, soberbia, sobremanera, sumo (sacerdote), con gran temor.*
[^RAE]: Diccionario de la Real Academia Española