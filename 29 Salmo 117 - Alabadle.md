# Salmo 117 - Alabadle

> **Salmo 117:1-2** Alabad a Jehová, naciones todas; Pueblos todos, alabadle. Porque ha engrandecido sobre nosotros su misericordia, Y la fidelidad de Jehová es para siempre. Aleluya.

En nuestro lenguaje hay oraciones clasificadas como **oraciones imperativas** y estas buscan expresar:

1. Un consejo
2. Un ruego o petición
3. Un mandado u orden
4. Una prohibición o negación

Precisamente el verso uno es una oración imperativa además de eso puede manifestar los cuatro puntos anteriores.

1. Un consejo porque se nos aconseja que alabemos a Jehová como una nación y como pueblo.
2. Un ruego o petición porque de parte del escritor del salmo anhela que alabemos a Jehová por varias razones pero una de ellas es que al no alabar a Jehová nuestra alabanza se va hacia los ídolos, dioses o señoríos y por consecuencia al alabar la maldad, nuestra destrucción vendría mas al alabar a Jehová nuestra restauración está propicia.
3. Un mandato u orden porque se nos manda u ordena al alabarlo porque él es el único merecedor de alabanza.
4. Una prohibición o negativa porque al indicar que le alabemos a él en ese mismo momento se está indicando que no se debe alabar a nadie más solo a él.

Pero en el verso 2 da un explicación del porque hay que alabarlo y por qué es imperativo hacerlo.

> **Salmo 117:2** Porque ha engrandecido sobre nosotros su misericordia, Y la fidelidad de Jehová es para siempre. Aleluya.

La palabra engrandecido en el original puede traducirse como esforzarse, es decir, Dios se esforzó para que nosotros tuviéramos sobre nuestras vidas su misericordia, ya que envió a su hijo Jesucristo dándole un cuerpo semejante al nuestro además de establecer la sombra de lo que Jesucristo iba a ser y enviar a todos sus profetas para el anuncio de la venida del Mesías y también como iba a rescatarnos de las tinieblas y el castigo eterno. Dejando todo establecido y al pie de la letra en cada verso, letra, revelación y figura de lo que Cristo iba a manifestar, lo cual es, el engrandecimiento de su misericordia, que ahora somos hijos a causa de el creer en Jesucristo y que murió en un madero para darnos salvación y perdón de pecados; resucitar al tercer día y proclamar victoria sobre la muerte.

Y en el siguiente texto dice «Y la fidelidad de Jehová es para siempre», es decir, su hijo Jesucristo es eterno, el nunca desaparecerá y la palabra dice que él siempre permanecerá fiel. 

Cuan grande es nuestro Dios que dejo a través de su siervo esta oración imperativa para que conozcamos y entendamos él porque de alabarle.

Así que Aleluya porque nos ha traído a la vida por medio de su hijo Jesucristo y porque ha engrandecido sobre nosotros su misericordia. ¡Gloria al Rey!