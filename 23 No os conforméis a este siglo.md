# No os conforméis a este siglo

> **Romanos 12:2** No os conforméis a este siglo, sino transformaos por medio de la renovación de vuestro entendimiento, para que comprobéis cuál sea la buena voluntad de Dios, agradable y perfecta.

En **Romanos 12:2** Cristo a través del apóstol Pablo, envía un mandamiento **No os conforméis a este siglo**. Mandamiento que no es nuevo sino basado en el que ya es ***Y amarás al Señor tu Dios con todo tu corazón, y con toda tu alma, y con toda tu mente y con todas tus fuerzas...***[^mandamiento]

En este versículo se expresan dos palabras, ***conformarse***[^conforme] y ***siglo***[^siglo], **conformarse** significa *participar y estar de acuerdo con un asunto y también tomar la formar de*, y **siglo** significa *un espacio de tiempo o mundo*.

Lo que el apóstol Pablo estaba diciendo era *No tomen la semejanza de este mundo (este tiempo)*, *No participen de este tiempo (este mundo)*, *No tengan la misma vida que tiene todo el mundo*. Porque al conformarnos a este siglo no cumplimos con el mandamiento de **amar con todo al Señor nuestro Dios** porque al conformarnos tenemos partes cimentadas en este siglo, sea nuestra mente, corazón, fuerzas, alma y al tener una parte en ello sea mucha o poca, no cumplimos el mandamiento que Jehová nuestro Dios nos estableció, porque él anhela ***Todo***. Al conformarnos a este siglo atentamos contra Dios, porque estamos divididos, tibios.

La palabra conforméis viene de **susjematízo**[^susjematizo] que significa *formar en forma parecida, esto es conformar al mismo patrón*. Pero se forma de dos palabras **sun**[^sun] y **sjéma**. **Sun**[^sun] denota unión, y **sjéma**[^sjema] figura.

Ejemplo de **sun**[^sun] lo podemos ver en:

> **Mateo 27:38** Entonces crucificaron **con** él a dos ladrones, uno a la derecha y otro a la izquierda.

Y **sjéma**[^sjema] lo podemos ver en dos pasajes:

> **1 Corintios 7:31** y los que disfrutan de este mundo, como si no lo disfrutasen; porque la **apariencia** de este mundo pasa.

> **Filipenses 2:8** Y estando en la **condición** de hombre, se humilló a sí mismo, haciéndose obediente hasta la muerte, y muerte de cruz.

Lo que escribió el apóstol Pablo es parte de la epístola a los Romanos, por lo que habla con una congregación no necesariamente a personas no creyentes sino aquellas que creen en el Mesías pero que necesitan apartarse de todo lo que en este mundo hay sea en corazón, mente, alma y fuerza. Y entregarse en la búsqueda del cumplimiento del primer mandamiento ***Y amarás al Señor tu Dios con todo tu corazón, y con toda tu alma, y con toda tu mente y con todas tus fuerzas...***[^mandamiento].

También en Romanos 12:2 se hace mención a *este siglo* es decir al tiempo en que vivimos (este tiempo), sabiendo que hay un *siglo venidero* un siglo que ha de venir, una forma de vida que vendrá que será completamente diferente al siglo en el que vivimos.[^siglos]

Lo que da a entender el apóstol Pablo al decir *No os conforméis a este siglo* es:

> Busquen el siglo venidero, busque la forma de vida que no es de este siglo.

Porque lo que corresponde a este siglo (sabiduría de este siglo) crucificó a Cristo Jesús[^sabestesiglo]. Porque lo que tiene este siglo es afán[^afan].

Con esto vemos que no debemos unirnos a este mundo para no tomar su apariencia, pero debemos unirnos a Jesucristo para participar de su muerte y por lo que al hacerlo moriremos completamente a este mundo y como consecuencia ya no podremos conformar. Y así tomar la apariencia de Cristo Jesús.

> **Mateo 27:38** Entonces crucificaron con él a dos ladrones, uno a la derecha y otro a la izquierda.

> **Gálatas 2:20** Con Cristo estoy juntamente crucificado, y ya no vivo yo, mas vive Cristo en mí; y lo que ahora vivo en la carne, lo vivo en la fe del Hijo de Dios, el cual me amó y se entregó a sí mismo por mí

> **Gálatas 6:14** Pero lejos esté de mí gloriarme, sino en la cruz de nuestro Señor Jesucristo, por quien el mundo me es crucificado a mí, y yo al mundo.

> **Filipenses 2:5-8** Haya, pues, en vosotros este sentir que hubo también en Cristo Jesús, el cual, siendo en forma de Dios, no estimó el ser igual a Dios, como cosa a que aferrarse, sino que se despojó a sí mismo, tomando forma de siervo, hecho semejante a los hombre; y estando en la condición de hombre, se humilló a sí mismo, haciéndose obediente hasta la muerte, y muerte de cruz.

[^mandamiento]: **Marcos 12:29** Jesús le respondió: El primer mandamiento de todos es: Oye, Israel; el Señor nuestro Dios, el Señor uno es.
[^sabestesiglo]: **1 Corintios 2:6-8** 6 Sin embargo, hablamos sabiduría entre los que han alcanzado madurez; y sabiduría, no de este siglo, ni de los príncipes de este siglo, que perecen.  7 Mas hablamos sabiduría de Dios en misterio, la sabiduría oculta, la cual Dios predestinó antes de los siglos para nuestra gloria, 8 la que ninguno de los príncipes de este siglo conoció; porque si la hubieran conocido, nunca habrían crucificado al Señor de gloria.
[^afan]: **Mateo 13:22** El que fue sembrado entre espinos, éste es el que oye la palabra, pero el afán de este siglo y el engaño de las riquezas ahogan la palabra, y se hace infructuosa.
[^siglos]: **Mateo 12:32** A cualquiera que dijere alguna palabra contra el Hijo del Hombre, le será perdonado; pero al que hable contra el Espíritu Santo, no le será perdonado, ni en este siglo ni en el venidero., Marcos 10:30 que no reciba cien veces más ahora en este tiempo; casas, hermanos, hermanas, madres, hijos, y tierras, con persecuciones; y en el siglo venidero la vida eterna.
[^susjematizo]: G4964 - **συσχηματίζω** - *susjematízo* - de G4862 y un derivado de G4976; formar en forma parecida, es decir, conformar al mismo patrón (figurativamente): *conformar*.
[^sun]: G4862 - **σύν** - *sun* - preposición primaria que denota unión; con o junto a (pero mucho más estrecha que G3326 o G3844), es decir, por asociación, compañía, proceso, parecido, posesión, instrumentalidad, adición, etc.: *con. [En composición tiene aplicaciones similares, incluso calidad de completo.]* 
[^sjema]: G4976 - **σχῆμα** - *sjéma* - del alternado de G2192; figura (como modo o circunstancia), es decir, (por implicación) externo, condición: *apariencia, condición*
[^conforme]: Definición basada en RAE: conformar - https://dle.rae.es/?id=AGUX3MR y Diccionario Strong G4964
[^siglo]: Definición basada en RAE: siglo - https://dle.rae.es/?id=Xr0ACdZ y Diccionario Strong G165