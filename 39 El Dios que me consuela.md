# El Dios que me consuela

> **Isaías 51:3** Ciertamente consolará Jehová a Sion; consolará todas sus soledades, y cambiará su desierto en paraíso, y su soledad en huerto de Jehová; se hallará en ella alegría y gozo, alabanza y voces de canto.

> **Isaías 51:12** Yo, yo soy vuestro consolador. ¿Quién eres tú para que tengas temor del hombre, que es mortal, y del hijo de hombre, que es como heno?

> **Juan 14:16** Y yo rogaré al Padre, y os dará otro Consolador, para que esté con vosotros para siempre:

Creo que mucho de lo que pasa en este mundo son aflicciones, tristezas y por consecuencia vivir desconsolados por la situación en la cual vivimos diariamente, pero realmente cuando comprendemos que tenemos un Dios Consolador podemos hallar consuelo en él.

Y no solo consuelo, sino que él ruega para que vayamos a él a recibir el consuelo que nos quiere dar.

También nos recuerda que él es nuestro consolador, así mismo para hallar un consuelo en él, envío a su hijo Jesucristo para traer y hacer vivo como fuego ardiente su consuelo a los hombres al ser Jesucristo la luz del mundo.

Y nos dio un consuelo muy grande al recibir la promesa del Espíritu Santo, el otro Consolador, el cual estará con nosotros para siempre.

Dios, quien es nuestro consolador, estará siempre al pendiente de ofrecernos consuelo a través de su palabra y presencia.

Así que acerquémonos al Dios que nos consuela y que nos recuerda que él es un Dios que consuela y que nos apacentará con su verdad, su palabra, y que no importa si estamos en soledades, desiertos u otras situaciones, sino que él a través de su consuelo cambiará nuestra situación a gozo, alegría, alabanza y voces de canto.

Así que acerquémonos con confianza al Dios que nos consuela.

> **Hebreos 4:16** Acerquémonos, pues, confiadamente al trono de la gracia, para alcanzar misericordia y hallar gracia para el oportuno socorro.