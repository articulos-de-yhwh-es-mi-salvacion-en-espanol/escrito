# ¡No temas, porque yo estoy contigo!

> **Isaías 41:10** No temas, porque yo estoy contigo; no desmayes, porque yo soy tu Dios que te esfuerzo; siempre te ayudaré, siempre te sustentaré con la diestra de mi justicia.

¡No temas! Porque Él está contigo, palabras de Dios a nosotros según lo habla Isaías, una de las primeras cosas que nos habla este versículo es sobre no temer porque su presencia está con nosotros. 

Jehová nos hace saber que está con nosotros que está presente en cada situación, y que por causa de olvidarnos de que Él está observado y que esta presente entramos en temor al ver a nuestro enemigo, el cual podemos verlo grande, imponente, con armas de guerra extrañas que pueden causar terror, pero luego se presenta el amado Jehová de los ejércitos, y nos dice «No temas, porque yo estoy contigo». 

Que nos quiere decir con esto, que Él es más imponente, más grande, y con la diestra de su justicia por arma de guerra ¡Aleluya!.

Así que no temas, porque el esta contigo y es tu Dios quien te esfuerza.