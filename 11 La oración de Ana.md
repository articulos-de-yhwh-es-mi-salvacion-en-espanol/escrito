# La oración de Ana

>**1 Samuel 1:13[^RV60]** Pero Ana hablaba en su corazón, y solamente se movían sus labios, y su voz no se oía; y Elí la tuvo por ebria.

El testimonio de Ana nos da una enseñanza muy preciosa, aunque lo que vivió pudo ser doloroso al ser una mujer estéril y tener una rival la cual tenía hijos y buscaba irritarla.

Esto pasaba todos los años y Ana se irritaba, se enojaba y se entristecía. Hasta que un día hizo lo siguiente:

>**1 Samuel 1:9-11[^RV60]** Y se levantó Ana después que hubo comido y bebido en Silo; y mientras el sacerdote Elí estaba sentado en una silla junto a un pilar del templo de Jehová, ella con amargura de alma oro a Jehová, y lloro abundantemente. E hizo voto, diciendo: Jehová de los ejercitos, si se dignares mirar a la aflicción de tu sierva, y te acordares de mí, y no te olvidares de tu sierva, sino que dieres a tu sierva un hijo de varón, yo lo dedicaré a Jehová todos los días de su vida, y no pasará navaja sobre su cabeza.

Y como consecuencia a esta oración Dios le dio un hijo al cual llamo Samuel y lo dedicó a Jehová. Además, hay un cántico muy hermoso que Ana dio a Jehová.[^Testimonio de Ana]

Creo que no debemos pasar por alto este testimonio porque nos dice mucho, y nos hace preguntar lo siguiente ¿Acaso todos esos años no oraba Ana por un hijo? ¿Acaso Dios no escuchaba a Ana? ¿Estaba Ana orando de una manera equivocada? ¿Acaso no lloraba Ana por causa de su esterilidad?

Creo sinceramente que Dios quería que Ana no solo diera a luz un hijo, sino que ella fuera para testimonio a todos nosotros, porque Dios quería darnos a conocer que a pesar de que nuestros enemigos nos echen en cara nuestra esterilidad, nuestra incapacidad de hacer las cosas o que no damos fruto, no importa lo que ellos digan sino que lo importante es la respuesta de Dios y su exaltación. 

Dios no solo quiere que demos fruto sino que todo el fruto que tengamos sea dedicado a Él, también vemos que Ana incurrió en una liberación porque derramo su alma delante de Dios y expuso su amargura delante de Dios.

Esto llevo a que Ana orara largamente delante de Dios, y tal vez las otras veces que Ana oraba solo hacia una petición y no incurrió en orar largamente ante Dios. 

También vemos que Ana oro desde su corazón tanto fue esto que solo su boca se movía y su corazón oraba.

Así que, como vemos, Dios no es que no oyera a Ana, ni es que Ana no oraba sino que Dios anhelaba que ella tuviera una liberación, un largo momento de intimidad con Dios, que todo el fruto que Ana tuviera fuera dedicado a Dios. Que Ana pudiera orar desde su corazón. Que Ana expresara un cántico tan hermoso que impactara a María.[^Cántico de Ana][^Cántico de Maria]

Tengamos en cuenta a Ana, porque Dios la tomo en cuenta y la puso por testimonio para que la imitemos en su actitud, una oración desde el corazón la cual debe ser derramada para dar a luz a Jesucristo y que todo nuestro fruto sea para Él. 

Dispongamos nuestro ser a imitar a Ana ya que el nombre de Ana significa «gracia», es decir, imitemos la gracia, veamos a la gracia, y oremos largamente delante de Dios y derramemos nuestro ser para que todo el fruto que vayamos a dar sea dedicado exclusivamente para Jehová ya que Él es el que nos oye y atiende a nuestra voz.

Y podremos decir «El me ha oído», «Su nombre es Dios» y de nosotros salga un cántico maravilloso para nuestro Dios, Jehová.

[^RV60]: Biblia Reina Valera 1960

[^Testimonio de Ana]: 1 Samuel 1
[^Cántico de Ana]: 1 Samuel 2:1-11
[^Cántico de Maria]: Lucas 1:46-55
[^Samuel]: Significa «Su nombre es Dios», «Oído de Dios»
[^Ana]: Significa Ana