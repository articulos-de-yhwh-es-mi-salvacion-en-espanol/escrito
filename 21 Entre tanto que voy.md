# Entre tanto que voy

> **1 Timoteo 4:13 SSEE[^ssee]** Entre tanto que voy, ocúpate en el leer, en exhortar y en enseñar.

En la epístola de 1 Timoteo, el apóstol Pablo escribe a Timoteo brindándole una serie de consejos e instrucciones. Siendo Pablo un ministro maduro y anciano, y Timoteo siendo joven y ministro le aconseja sobre lo que debe hacer mientras él llega y entre una de sus consejos le dice *ocúpate* es decir **no estés ocioso**, se diligente además de esto agrega **en el leer, en exhortar y en enseñar**.

De una instrucción general que es **ocúpate** paso a una instrucción específica **en el leer, en exhortar y en enseñar**.

Por otro lado vemos que el apóstol Pablo le dice que pronto irá a verlo, y su anhelo es que él siga este consejo tanto para su edificación (la de Timoteo) como para la edificación de la Iglesia.

Al leer 1 Timoteo podemos darnos cuenta que Pablo trata a Timoteo como a un hijo por lo que Pablo toma la posición de un padre.[^hijo]

Con lo escrito anteriormente podemos darnos cuenta de tres cosas que se hablan en 1 Timoteo 4:13:

1. Una instrucción de un padre a un hijo.[^hijo]
2. Una instrucción de un anciano a un joven.[^joven]
3. Una instrucción de un ministro a otro ministro.[^ministro]

Al observar estas tres facetas, nos podemos dar cuenta que la instrucción procede de Jesucristo hacia nosotros ya que toda escritura es inspirada por Dios y siendo él nuestro Padre nos deja instrucciones como un padre lo hace con un hijo, también como un anciano a un joven y de un ministro a otro ministro. Siendo Jesucristo un Padre, un Anciano, y un Ministro.[^tresfacetas]

Por lo que en este verso Jesucristo nos dice: *"Entre tanto que voy..."*, es decir *en lo que llega tu visitación*. Con esto nos está diciendo que nos visitará pero anhela que estemos ocupados **en el leer, en exhortar y en enseñar**. Anhela encontrarnos en esta actitud.

Al ver la palabra *ocúpate* en griego es **proséjo**[^prosejo] que significa prestar atención (figurativamente: sostener la mente), lo que nos da a entender es que debemos prestar atención **al leer, exhortar y enseñar**, y que esto sostendrá nuestra mente siendo un pensamiento espiritual, nos formará espiritualmente, para ser espirituales.

> **Romanos 8:5** Porque los que son de la carne piensan en las cosas de la carne; pero los que son del Espíritu, en las cosas del Espíritu.

Leer, exhortar y enseñar son palabras que debemos conocer.

1. ***Leer*** en el verso es la palabra griega **anágnosis**[^anagnosis] que significa *leer*. **anágnosis**[^anagnosis] proviene de una palabra compuesta **anáginósko**[^anaginosko] que significa *aprender de nuevo*.
2. ***Exhortar*** en el verso es la palabra griega **paráklesis**[^paraklesis] que significa imploración, exhortación o solaz. **paráklesis**[^paraklesis] proviene de una palabra compuesta **parakaléo**[^parakaleo] que significa *llamar cerca (invitar, invocar)*.
3. ***Enseñar*** en el verso es la palabra griega **didaskalía**[^didaskalia] que significa *instrucción*. **didaskalía**[^didaskalia] proviene de la palabra **didáskalos**[^didaskalos] que significa *instructor*.

En el verso de Timoteo *leer* vemos que es un acto que debemos hacer pero también al leer somos exhortados y enseñados. Por lo que al recibir esta exhortación y enseñanza de parte de las escrituras, nos llevará a impartir la exhortación y enseñanza recibida.[^leer]

El exhortar como se ve en el griego tiene también la aplicación de *implorar* aunque regularmente se puede aplicar hacia una persona (la exhortación, imploración), debemos darnos cuenta que exhortar es según la RAE[^rae].

> Incitar a alguien con palabras, razones y ruegos a que haga o deje de hacer algo.

La palabra implorar significa según la RAE[^rae]:
> Pedir con ruegos o lágrimas algo.

Podemos exhortar o implorar a Dios para que obre en nuestras vidas, y que también debemos rogarle con lágrimas para que su obra se haga manifiesta, ya sea una respuesta, un consejo, dirección etc. Y por el otro lado también es el hecho que roguemos a otros para el cambio.[^exhortar]

Al hablar de enseñanza también vemos que debemos enseñarnos a nosotros mismo y así mismo enseñar lo aprendido. En 1 Timoteo 4:16 también se hace referencia a la enseñanza (la misma palabra griega) pero con la palabra doctrina, y nos da una advertencia *"Ten cuidado de ti mismo y de la `doctrina`...".*[^enseniar].

La instrucción de leer, exhortar y enseñar se puede ver completa en Hechos 13:15-41.

Así que debemos ocuparnos en el leer, exhortar y edificar en lo que nuestra visitación viene.

[^ssee]: Biblia Sagradas Escrituras
[^hijo]: 1 Timoteo 1:2,18
[^joven]: 1 Timoteo 4:12
[^ministro]: 1 Timoteo 4:6
[^prosejo]: G4337 - **προσέχω** - _proséjo_ - de G4314 y G2192; (figurativamente) sostener la mente (G3563 implicación) hacia, es decir, prestar atención a, tener cautela por, aplicarse uno mismo a, adherirse a: _oír, servir, escuchar, guardarse, atender, atento, dado._
[^anagnosis]: G320 - **ἀνάγνωσις** - _anágnosis_ - de G314; (el acto de) leer: _lectura, leer_.
[^anaginosko]: G314 - **ἀναγινώσκω** - _anaginósko_ - de G303 y G1097; aprender de nuevo, es decir, (por extensión) leer: _leer_.
[^paraklesis]: G3874 - **παράκλησις** - _paráklesis_ - de G3870; implicación, exhortación, solaz: _exhortación, confortar, consolación, consolar, consuelo_.
[^parakaleo]: G3870 - **παρακαλέω** - _parakaléo_ - de G3844 y G2564; llamar cerca, es decir, invitar, invocar (por imploración, exhortación o consolación): _orar, presentar, rogar, alentar, amonestar, animar, confortar, consolación, consolar, exhortación, exhortar, exigencia._
[^didaskalia]: G1319 - **διδασκαλία** - _didaskalía_ - de G1320; instrucción (la función o la información): _enseñanza, enseñar, doctrina._
[^didaskalos]: G1320 - **διδάσκαλος** - _didáskalos_ - de G1321; instructor (generalmente o específicamente): _doctor, maestro, padre (de familia)_.
[^leer]: Hechos 13:15, 2 Timoteo 3:16
[^rae]: Real Academia Española
[^exhortar]: 2 Corintios 5:20
[^enseniar]: Romanos 2:21, 1 Timoteo 4:16
[^tresfacetas]: Isaías 9:6, Daniel 7:9, Romanos 15:8, Gálatas 2:17