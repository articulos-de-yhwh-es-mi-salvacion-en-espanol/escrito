# Oh muerte, yo seré tu muerte

> **Génesis 1:1** En el principio creó Dios los cielos y la tierra

> **Oseas 13:14** De la mano del Seol los redimiré, los libraré de la muerte. Oh muerte, yo seré tu muerte; y seré tu destrucción, oh Seol; la compasión será escondida de mi vista.

> **Mateo 27:33** Y cuando llegaron a un lugar llamado Gólgota, que significa: Lugar de la Calavera;

Dios prometió destruir a la muerte, porque él dijo «Oh muerte, yo seré tu muerte; y seré tu destrucción, oh Seol», para ello Dios hizo uso de su Hijo Jesucristo, teniendo en cuenta que Dios es un Dios creador, pero para este caso Cristo llegó a una dimensión para destruir y luego crear un Génesis, porque al lugar donde llego fue el de la Calavera, a la dimensión de la muerte, llego hasta su cabeza y clavo la cruz en la calavera, es decir, en la parte más alta, desde la cabeza Cristo Jesús venció a la muerte. ¡Aleluya!

> **Juan 19:34** Pero uno de los soldados le abrió el costado con una lanza, y al instante salió sangre y agua.

Y no solo clavo la cruz dando una victoria sobre la muerte, sino que también derramo su sangre cumpliendo aquellas palabras que dijo David:

> **2 Samuel 1:16** Y David le dijo: Tu sangre sea sobre tu cabeza, pues tu misma boca atestiguó contra ti, diciendo: Yo maté al ungido de Jehová.

También Jesucristo derramó agua y se cumplieron las siguientes palabras sobre la muerte:

> **Lamentaciones 3:54** Aguas cubrieron mi cabeza; yo dije: Muerto soy

Así Jehová declaró y cumplió la palabra sobre la muerte a través de su Hijo Jesucristo teniendo toda victoria sobre la muerte. ¡Aleluya!

