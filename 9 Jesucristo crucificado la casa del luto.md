# Jesucristo crucificado, la casa del luto

> **Eclesiastés 7:2[^RV60]** Mejor es ir a la casa del luto que a la casa del banquete; porque aquello es el fin de todos los hombres, y el que vive lo pondrá en su corazón.

Una casa es un lugar que esta diseñada para ser habitada, en Eclesiastés dice: «mejor es ir a la casa del luto», es decir, mejor es ir a habitar en el lugar donde hay luto, porque aquello es el fin de todos los hombres.

Jesucristo crucificado significa la casa del luto porque aquello (Jesucristo crucificado) es el fin de todos los hombres por una parte de aquellos que deben ser crucificados juntamente con Él, y por otra parte aquellos que no creen en Jesucristo su fin es de muerte porque Cristo Jesús al ser crucificado expuso la muerte de aquellos que no creerían en Él.

Debemos tomar la actitud del apóstol Pablo que dijo:
> **1 Corintios 2:2[^RV60]** Pues me propuse no saber entre vosotros cosa alguna sino a Jesucristo, y a éste crucificado.

Es decir, al meditar en Jesucristo y este crucificado estamos habitando en la casa del luto porque velamos a Cristo Jesús observamos que el fin de todos los hombres era morir crucificados en una muerta que la ocasiono el pecado, nuestra iniquidad y rebeldía.

Pero ahora al Jesucristo tomar nuestro lugar y dar su vida, ya no debemos pasar por esa muerte que le debía acontecer a todos los hombres, una muerte fatal y despiadada en la cual Él mismo Dios se apartaría de nosotros independientemente de quienes fuéramos porque de su hijo en el momento de su crucifixión se tuvo que apartar porque Jesucristo ya era pecado.

Al velar y ver lo que nos acontecería y ver que él tomó nuestro lugar, un dolor puede llenar nuestro corazón pero también un gran gozo de saber que Él tomó nuestro lugar y que ahora somos redimidos de toda muerte.

> **Salmos 30:11[^RV60]** Has cambiado mi lamento en baile, Desataste mi cilicio, y me ceñiste de alegría.

Y para todo aquel que quiera creer en Jesucristo un gozo grande le espera y una Paternidad.

Además, sabemos que al meditar en Jesucristo y este crucificado vemos que el fin de todo hombre es este «el morir crucificado» porque necesitamos ser crucificados juntamente con Cristo todos los días tomar la cruz y seguirlo.

Y para aquel que no ha creído también le espera una crucifixión porque Jesucristo tomo el lugar de todo el que cree en Él, y aquel que no ha creído será crucificado con el mundo.

>**Gálatas 6:14[^RV60]** Pero lejos esté de mí gloriarme, sino en la cruz de nuestro Señor Jesucristo, por quien el mundo me es crucificado a mí, y yo al mundo.

«Y el que vive lo pondrá en su corazón», al meditar y habitar en la casa del luto nos damos cuenta que nuestro fin es ser crucificados juntamente con Él y que al entrar en la vida de nuestro salvador guardaremos este acto que Jesucristo hizo y lo guardaremos en nuestro corazón.

> **Lucas 9:23-24[^RV60]** Y decía a todos: Si alguno quiere venir en pos de mí, niéguese a sí mismo, tome su cruz cada día, y sígame. Porque todo el que quiera salvar su vida, la perderá; y todo el que pierda su vida por causa de mí, éste la salvará.

[^RV60]: Biblia Reina Valera 1960