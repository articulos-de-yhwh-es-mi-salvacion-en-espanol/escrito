# Velando sobre la palabra

> **Jeremías 1:11-12 Ausejo**[^Ausejo] La palabra de Yahveh me fue dirigida en estos términos: "¿Qué ves, Jeremías?". Respondí: "Estoy viendo una rama de almendro". Yahveh me dijo: "Bien has visto; porque yo estoy velando por mi palabra para cumplirla".

> **Efesios 5:1 RV60**[^RV60] Sed pues, imitadores de Dios como hijos amados.

Hay algo que se deja ver muy claro en Jeremías 1:12 y es que Dios es vigilante, sin dar lugar al sueño para cumplir su palabra, para ponerla por obra.

Así mismo debemos imitarle y velar sobre su palabra para ponerla por obra y no dormirnos en el cumplimiento de la misma, sino estar atentos.

La traducción exacta de «velando» en el original es «estar alerta», es decir, debemos estar alerta a la palabra que Dios nos ha dado para ponerla por obra.

También en el versículo anterior al 12 dice:

> **Jeremías 1:11 Ausejo**[^Ausejo] La palabra de Yahveh me fue dirigida en estos términos: "¿Qué ves, Jeremías?". Respondí: "Estoy viendo una rama de almendro".

La palabra almendro en el hebreo es «shaquéd»[^shaquéd] que proviene de la raíz hebrea «shacad»[^shacad] y significa *estar alerta* o *apresurarse*, por esto al árbol de almendro le decían el «apresurado» porque era el primero en florecer en la primavera. Y la raíz de la palabra almendro es la misma que se usa en el versículo siguiente para «velar» por ello en algunas traducciones lo traducen «apresurar»

> **Jeremías 1:12 RV60**[^RV60] Y me dijo Jehová: Bien has visto; porque yo **apresuro** mi palabra para ponerla por obra.

Busquemos apresurarnos a cumplir su palabra porque él que lo ama cumple con sus mandamientos.

> **Efesios 5:14 RV60**[^RV60] Por lo cual dice: Despiértate, tú que duermes, Y levántate de los muertos, Y te alumbrará Cristo.

> **Salmo 40:7-8 RV60**[^RV60] Entonces dije: He aquí, vengo; En el rollo del libro está escrito de mí; El hacer tu voluntad, Dios mío, me ha agradado, Y tu ley está en medio de mi corazón.

> **Deuteronomio 17:19 RV60**[^RV60] y lo tendrá consigo, y leerá en él todos los días de su vida, para que aprenda a temer a Jehová su Dios, para guardar todas las palabras de esta ley y estos estatutos, para ponerlos por obra;

[^shaquéd]: ***H8247*** -  **שָׁקֵד** - *shaquéd* - de H8245; almendro (el árbol o la nuez, siendo la más temprana para florecer): *almendra, almendro*
[^shacad]: ***H8245*** - **שָׁקַד** - *shacad* - raíz primaria; estar alerta, es decir, insomne; de aquí, estar de vigilia (sea para bien o para mal): *cuidado, desvelar, velar, vigilar*.
[^Ausejo]: Biblia Serafin Ausejo
[^RV60]: Biblia Reina Valera 1960