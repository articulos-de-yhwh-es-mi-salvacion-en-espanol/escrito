# ¿Qué honra o qué distinción se hizo por esto?

> **Ester 6:1-3** **1** Aquella misma noche se le fue el sueño al rey, y dijo que le trajesen el libro de las memorias y crónicas, y que las leyeran en su presencia. **2** Entonces hallaron escrito que Mardoqueo había denunciado el complot de Bigtán y de Teres, dos eunucos del rey, de la guardia de la puerta, que habían procurado poner mano en el rey Asuero. **3** Y dijo el rey: ¿Qué honra o qué distinción se hizo a Mardoqueo por esto? Y respondiendo los servidores del rey, sus oficiales: Nada se ha hecho con él.

Déjame decirte que Dios no se ha olvidado de ti o de las justicias que has hecho en su nombre para salvación, puede que no hayas recibido ninguna honra o distinción por eso, pero está escrito en su libro de memorias y crónicas. Y en cualquier momento Dios pedirá que se lea ese libro en su presencia y al oír lo que has hecho él dirá ***¿Qué honra o qué distinción se hizo a «tu nombre» por esto?***. 

Dios no es que se olvide sino que espera el momento perfecto para honrarte como se debe porque:

> **Proverbios 22:4** Riquezas, honra y vida son la remuneración de la humildad y del temor de Jehová.

> **Ester 6:4-7** **4** Entonces dijo el rey: ¿Quién está en el patio? Y Amán había venido al patio exterior de la casa real, para hablarle al rey para que hiciese colgar a Mardoqueo en la horca que él le tenía preparada. **5** Y los servidores del rey le respondieron: He aquí Amán está en el patio. Y el rey dijo: Que entre. **6** Entró, pues, Amán, y el rey le dijo: ¿Qué se hará al hombre cuya honra desea el rey? Y dijo Amán en su corazón: ¿A quién deseará el rey honra más que a mí? **7** Y respondió Amán al rey: Para el varón cuya honra desea el rey, **8** traigan el vestido real de que el rey se viste, y el caballo en que el rey cabalga, y la corona real que está puesta en su cabeza; **9** y den el vestido y el caballo en mano de alguno de los príncipes más nobles del rey, y vistan a aquel varón cuya honra desea el rey, y llévenlo en el caballo por la plaza de la ciudad, y pregonen delante de él: Así se hará al varón cuya honra desea el rey. **10** Entonces el rey dijo a Amán: Date prisa, toma el vestido y el caballo, como tú has dicho, y hazlo así con el judío Mardoqueo, que se sienta a la puerta real no omitas nada de todo lo que has dicho.

Si vemos en los versos anteriores un enemigo de Mardoqueo que lo quería matar, llego delante del rey para pedir la muerte de Mardoqueo, más no sucedió de esa manera porque al rey se le había leído el libro de las memorias y crónicas, y esto provoco que esa sentencia de muerte que venia contra Mardoqueo, se convirtiera en una sentencia de honra.

Así que no creas que Dios se ha olvidado de tus justicias sino que espera el momento indicado para que esa justicia te salve de tus enemigos y honrarte en el momento perfecto.

> **Salmos 23:4-6** **4** Aunque ande en valle de sombra de muerte, No temeré mal alguno, porque tú estarás conmigo; Tu vara y tu cayado me infundirán aliento. **5** Aderezas mesa delante de mí en presencia de mis angustiados; Unges mi cabeza con aceite; mi copa está rebosando. **6** Ciertamente el bien y la misericordia me seguirán todos los días de mi vida, Y en la casa de Jehová moraré por largos días.

> **Ester 6:11** Y Amán tomó el vestido y el caballo, y vistió a Mardoqueo, y lo condujo a caballo por la plaza de la ciudad, e hizo pregonar delante de él: Así se hará al varón cuya honra desea el rey.

Así que no lo olvides, todas tus justicias Dios las tiene escritas en su libro de memorias y crónicas y solo está esperando el tiempo preciso para honrarte.

> **Habacuc 2:3-4** **3** Aunque la visión tardará aún por un tiempo, mas se apresura hacia el fin, y no mentirá; aunque tardare, espéralo, porque sin duda vendrá, no tardará. **4** He aquí que aquel cuya alma no es recta, se enorgullece; mas el justo por su fe vivirá.