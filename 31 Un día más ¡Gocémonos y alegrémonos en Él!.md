# Un día más ¡Gocémonos y alegrémonos en Él!

> **Salmos 118:24** Este es el día que hizo Jehová; Nos gozaremos y alegraremos en él.

Creo que hay días en los cuales nos levantamos decepcionados, frustrados o en cualquier estado que no es muy grato pero creo que debemos enfocar nuestra vida en esos momentos hacia Cristo Jesús.

El Salmo 118:24 es muy hermoso porque nos dice que este día Dios lo ha hecho y lo ha hecho para que nos gocemos y alegremos en él, no para que andemos cabizbajos y lamentándonos de cosas que no tienen sentido o que le hemos dado un sentido de importancia mayor al de gozarnos y alegrarnos en nuestro Hacedor.

> **Hebreos 11:25** escogiendo antes ser maltratado con el pueblo de Dios, que gozar de los deleites temporales del pecado,

Tampoco es un día para pensar en pecar o buscar gozarnos o alegrarnos en el pecado, ¡Claro que no! El texto dice «Nos gozaremos y alegraremos en él», es decir, en el amor porque él nos ama, en la justicia porque ha dado a su hijo unigénito para que diera su vida en la cruz para la remisión de nuestros pecados, en la santidad porque nos ha concedido ser como Él, porque él es santo y el manda que seamos santos.

Así que herman@ y amig@ ¡Gocémonos y alegrémonos en Él porque este es el día que ha hecho Jehová! ¡Prorrumpa de júbilo porque nuestro Hacedor nos dará un corazón y espíritu nuevo! ¡Esperanza tenemos en Cristo Jesús ya que por causa de él nos han dado el Espíritu de Santidad para guiarnos a toda verdad y ser llenos de esperanza! ¡Gocémonos porque en este día Dios buscará santificarnos en espíritu, alma y cuerpo para el día de su venida! 

Así que ¡Gocémonos y alegrémonos en Él! ¡Gloria al Rey!

> **Ezequiel 11:19** Y les daré un corazón, y un espíritu nuevo pondré dentro de ellos; y quitaré el corazón de piedra de en medio de su carne, y les daré un corazón de carne,

> **Romanos 15:13** Y el Dios de esperanza os llene de todo gozo y paz en el creer, para que abundéis en esperanza por el poder del Espíritu Santo.

> **Juan 16:13** Pero cuando venga el Espíritu de verdad, él os guiará a toda la verdad; porque no hablará por su propia cuenta, sino que hablará todo lo que oyere, y os hará saber las cosas que habrán de venir.

> **Isaías 61:3** a ordenar que a los afligidos de Sion se les dé gloria en lugar de ceniza, óleo de gozo en lugar de luto, manto de alegría en lugar del espíritu angustiado; y serán llamados árboles de justicia, plantío de Jehová, para gloria suya.

> **1 Tesalonicenses 5:23** Y el mismo Dios de paz os santifique por completo; y todo vuestro ser, espíritu, alma y cuerpo, sea guardado irreprensible para la venida de nuestro Señor Jesucristo.

> **Juan 16:33** Estas cosas os he hablado para que en mí tengáis paz. En el mundo tendréis aflicción; pero confiad, yo he vencido al mundo.