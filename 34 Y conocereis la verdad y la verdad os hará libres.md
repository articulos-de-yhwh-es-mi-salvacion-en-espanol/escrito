# Y conoceréis la verdad, y la verdad os hará libres

> **Juan 8:32** y conoceréis la verdad, y la verdad os hará libres.

Bendiciones a todos, espero que el amor de Dios fluya en sus corazones y que la gracia de Dios les sostenga.

En Juan 8:32 tenemos una gran promesa y la promesa es que si conocemos la verdad, la verdad nos hará libres.

Y no solo esto, sino que en Juan 8:36 dice lo siguiente: 

> **Juan 8:36** Así que, si el Hijo os libertare, seréis verdaderamente libres.

Es decir, no solo tenemos la promesa de libertad sino la certeza de una libertad plena y verdadera.

Y esto es a consecuencia del sacrificio expiatorio de Cristo Jesús que al resucitar nos dio libertad de la muerte, es decir, todos estábamos a expensas de la muerte y regidos bajo ese régimen de esclavitud.

Y esta muerte tenía como arma el pecado y como poder la ley.

Pero Cristo al morir en la cruz venció al pecado en sí mismo y cumplió la ley para obtener victoria sobre ellos y hacernos libres de ellos y de las consecuencias que ambos nos daban.

Porque Cristo Jesús sufrió la consecuencia de la ley y del pecado. Pero ahora por su muerte y resurrección, no solo venció a la ley y al pecado sino a la muerte. Y por consecuencia tenemos una plenitud de libertad en Cristo Jesús.

Así que ten esperanza en que Cristo tiene la verdadera libertad y las llaves para sacarte de todas tus prisiones.

> **Romanos 8:2** Porque la ley del Espíritu de vida en Cristo Jesús me ha librado de la ley del pecado y de la muerte.

> **1 Corintios 15:56** ya que el aguijón de la muerte es el pecado, y el poder del pecado, la ley.

> **Apocalipsis 3:7-8** Escribe al ángel de la iglesia en Filadelfia: Esto dice el Santo, el Verdadero, el que tiene la llave de David, el que abre y ninguno cierra, y cierra y ninguno abre: Yo conozco tus obras; he aquí, he puesto adelante de ti una puerta abierta, la cual nadie puede cerrar; porque aunque tienes poca fuerza, has guardado mi palabra, y no has negado mi nombre.