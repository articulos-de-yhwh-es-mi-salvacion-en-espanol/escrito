# Estoy velando sobre mi palabra

> **Jeremías 1:12  Ausejo**[^Ausejo]  Yahveh me dijo: "Bien has visto; porque yo estoy velando por mi palabra para cumplirla".

Jehová le dice a Jeremías que él está velando sobre su palabra para ponerla por obra, esto es maravilloso porque Él nos ha dado promesas, ha escrito mandamientos en nuestro corazón, ha dado palabra de aliento y de vida a nuestro ser, ha puesto el Espíritu de su Hijo en nosotros, el cual es Cristo (La Palabra) y está velando para que toda la palabra que él ha puesto en nosotros se cumpla por eso mismo dice en un verso:

> **2 Pedro 3:9 RV60**[^RV60] El Señor no retarda su promesa, según algunos la tienen por tardanza, sino que es paciente para con nosotros, no queriendo que ninguno perezca, sino que todos procedan al arrepentimiento.

Es decir, él está alerta, atento a lo que ha puesto en nosotros para que toda palabra sea cumplida. En el original «velando» sé pude traducir como «insomne», es decir, que él no duerme, él siempre está atento a lo que ha puesto en nosotros, y se apresura a cumplir su palabra en nosotros, solo, no debemos tenerlo por tardanza, sino saber que él está alerta.

> **Salmos 121:3-4 RV60**[^RV60] No dará tu pie al resbaladero, Ni se dormirá el que te guarda. He aquí, no se adormecerá ni dormirá El que guarda a Israel.

> **Jeremías 1:12 RV60**[^RV60] Y me dijo Jehová: Bien has visto: porque yo apresuro mi palabra para ponerla por obra.

> **Habacuc 2:3 RV60**[^RV60] Aunque la visión tardará aún por un tiempo, mas se apresura hacia el fin, y no mentirá; aunque tardare, espéralo, porque sin duda vendrá, no tardará.

[^Ausejo]: Biblia Serafin Ausejo
[^RV60]: Biblia Reina Valera 1960

