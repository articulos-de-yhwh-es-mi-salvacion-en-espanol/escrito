# La influencia de un padre justo

> **Mateo 1:1** Libro de la genealogía de Jesucristo, hijo de David, hijo de Abraham.

El libro de Mateo comienza indicando dos paternidades de Jesucristo, una es Abraham y la otra es David.

Pero si observamos no es una paternidad de engendramiento como la de Abraham con Isaac o David y Salomón sino que es espiritual de acá es donde se manifiesta «**la influencia**».

Influencia es:
1. Poder de una persona o cosa para determinar o alterar la forma de pensar o de actuar de alguien.
2. Efecto, consecuencia o cambio que produce una cosa en otra.
3. Manifestación en una cosa de la influencia de otra.

Es decir, que una paternidad es una influencia de dar un carácter espiritual conforme a la palabra de Dios.

Mas Abraham y David se caracterizaron por ser justos delante de Dios cada quien basado en ciertas características por una parte Abraham con la fe y por otra David con el amor.

Estas cualidades los hicieron justos delante de Dios:
> **Santiago 2:23** Y se cumplió la Escritura que dice: **Abraham creyó a Dios**, y le fue contado por **justicia**, y fue llamado amigo de Dios.

> **Salmo 18:1** (Salmo de David) **Te amo, oh Jehová**, fortaleza mía.

> **1 Reyes 3:3** Mas Salomón amó a Jehová, andando en los estatutos de su **padre David**; solamente sacrificaba y quemaba incienso en los lugares altos.

> **2 Samuel 8:15** Y reinó **David** sobre todo Israel; y **David** administraba **justicia** y equidad a todo su pueblo.

Por esto mismo, por ser padres y justos, Dios estableció pacto con cada uno. Por una parte con Abraham, Dios estableció pacto y dio la promesa de la simiente, y con David se estableció el pacto del reinado eterno.

> **Gálatas 3:16** Ahora bien, a **Abraham** fueron hechas las promesas, y a su **simiente**. No dice: Y a las simientes, como si hablase de muchos, sino como de uno: Y a tu **simiente**, la cual es **Cristo**.

> **2 Samuel 7:12** Y cuando tus días sean cumplidos, y duermas con tus padres, yo levantaré después de ti a uno de tu linaje, el cual procederá de tus entrañas, y afirmaré su reino.

Tanto por la fe y el amor de estos dos padres justos generaron una influencia de tres maneras:
1. Sus hijos fueron cimentados conforme a los padres, como en el caso de Isaac con Abraham al ser su hijo obediente y seguir la voluntad del su padre el cual lo iba a sacrificar. David que su hijo Salomón amó a Dios y siguió el camino de su padre y su reinado fue de paz. Génesis 22

2. A todos aquellos que creen en lo que ellos creyeron y siguen su ejemplo serán sus hijos. 

   > **Juan 8:39** Respondieron y le dijeron: Nuestro padre es Abraham. Jesús les dijo: Si fueseis hijos de Abraham, las obras de Abraham haríais.

3. Ambos dieron lugar a un engendramiento espiritual y al cumplimiento de una promesa llamada Jesucristo. 

   > **Mateo 1:1** Libro de la genealogía de Jesucristo, hijo de David, hijo de Abraham.

Abraham representa a Dios Padre el cual por la fe estipula todo lo que es deseado y luego se manifiesta Jesucristo el consumador de la fe porque cumple con toda la visión de la justicia de Dios, convirtiendo a su hijo en pecado para que todos aquellos que crean en Jesucristo sean hechos justicia.

> **2 Corintios 5:21** Al que no conoció pecado, por nosotros lo hizo pecado, para que nosotros fuésemos hechos justicia de Dios en él.

David representa el reinado de Jehová el cual destruye a todos los enemigos para que haya justicia y equidad para establecer a su hijo el cual establecerá un lugar de adoración para alcanzar el conocimiento de Dios.

Ahora para que Jesucristo pudiera ser justo, vio y oyó a su Padre el cual es justo y aprendió, y obtuvo de esta manera su conocimiento.

> **Juan 17:25** Padre justo, el mundo no te ha conocido, pero yo te he conocido, y éstos han conocido que tú me enviaste.

> **1 Juan 2:1** Hijitos míos, estas cosas os escribo para que no pequéis; y si alguno hubiere pecado, abogado tenemos para con el Padre, a Jesucristo el justo.

Por conclusión debemos dejarnos influenciar por la paternidad del justo Jesucristo para seguir su ejemplo y ser como Él es.

> **Proverbios 20:7** Camina en su integridad el justo; Sus hijos son dichosos después de él.

> **Gálatas 3:16** Ahora bien, a Abraham fueron hechas las promesas, y a su simiente. No dice: Y a las simientes, como si hablase de muchos, sino como de uno: Y a tu simiente, la cual es Cristo.

> **Génesis 12:7** Y apareció Jehová a Abram, y le dijo: A tu descendencia daré esta tierra. Y edificó allí un altar a Jehová, quien le había aparecido.

> **Juan 8:39** Respondieron y le dijeron: Nuestro padre es Abraham. Jesús les dijo: Si fueseis hijos de Abraham, las obras de Abraham haríais.