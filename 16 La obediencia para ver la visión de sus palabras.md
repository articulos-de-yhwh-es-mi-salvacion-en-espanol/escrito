# La obediencia para ver la visión de sus palabras

> **Jeremías 18:1-2[^RV60]** Palabra de Jehová que vino a Jeremías, diciendo: Levántate y vete a casa del alfarero, y allí te haré oír mis palabras.

Dios Jehová en su infinita misericordia habla a nuestras vidas para enseñarnos, consolarnos, gozarnos y venga paz a nuestras vidas. Y busca que comprendamos su propósito y que recobre ánimo nuestra alma.

En el caso de Jeremías, Dios se acerca y le dice «Levántate y vete a casa del alfarero y allí te haré oír mis palabras», es decir, Dios anhela hablarnos con todo el corazón, pero busca momentos precisos y proféticos para plasmar su visión en nosotros por ello nos habla para que al obedecer entremos a un lugar en el cual su palabra se haga manifiesta y comprendamos a plenitud sus palabras.

> **Jeremías 18:3-6[^RV60]** Y descendí a casa del alfarero, y he aquí que él trabajaba sobre la rueda. Y la vasija de barro que él hacía se echó a perder en su mano; y volvió y la hizo otra vasija, según le pareció mejor hacerla. Entonces vino a mí palabra de Jehová diciendo: ¿No podré yo hacer de vosotros como este alfarero, oh casa de Israel? dice Jehová. He aquí que como el barro en la mano del alfarero, así sois vosotros en mi mano, oh casa de Israel.

Por esto vemos que Dios le da la orden a ir a la casa del alfarero a Jeremías para enseñarle a través de una visión y con esto la llegada de su voz para adoctrinar[^adoctrinar] conforme a su visión.

A veces, podemos pensar «¿porqué Dios no me habla?», solo recuerda o ruega para que Él te recuerde que fue lo que ya te ha dicho que debes cumplir para que su voz se haga manifiesta en una dimensión profética en la cual veas la labor de Dios y te manifieste lo que hay en su corazón, y te de la esperanza de su restauración y visión.

Busca la obediencia de su palabra y veras la visión de sus palabras.

[^adoctrinar]: Enseñar o educar a alguien en una doctrina, inculcándole determinadas ideas o creencias.