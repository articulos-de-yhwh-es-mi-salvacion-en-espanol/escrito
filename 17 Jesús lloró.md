# Jesús lloró

> **Juan 11:35 [^RV60] ** Jesús lloró

¿Qué tan importante es llorar para que Dios nos haya dado un ejemplo de esto? ¿Por qué Dios tomó a su hijo y le dio la facultad de expresar un llanto y que esto haya quedado registrado en la escritura?

Lo único que nos indica esto es que llorar es algo muy importante para Dios por esto mismo dejó un ejemplo en la escritura por medio de su Hijo Amado, el cual lloró.

Creo que, a veces solo reprimimos nuestras lágrimas por no querer sentir algún sentimiento de tristeza o vulnerabilidad, o nos enseñan que llorar no es para hombres o es un síntoma de debilidad. Cuando el más poderoso, hermoso, respetuoso y gozoso, lloró, la palabra dice que debemos ser imitadores de Cristo pero creo que a veces solo nos enfocamos en el poder, en actitudes como predicar o profetizar, pero ¿por qué no imitar el llorar?

Llorar es una expresión de libertad y no debemos sentirnos débiles por esto, todo lo contrario, es de fuertes llorar ¿por qué? Porque tenemos la fortaleza para expresar nuestro dolor, tristeza, frustración, alegría etc. por medio del llanto.

Llorar es una expresión de liberación, Dios nos da la capacidad para llorar, usémosla, y desahoguemos todo aquello que está dentro de nosotros por medio del llanto e imitemos a Cristo Jesús en esto, en llorar.



> **1 Corintios 11:1[^RV60]** Sed imitadores de mí, así como yo de Cristo.



Oremos y roguemos a Dios para poder llorar y que quité todo orgullo de nuestro corazón para poder llorar.



> **Salmos 30:5[^RV60]** Porque un momento será su ira, Pero su favor dura toda la vida. Por la noche durará el lloro, Y a la mañana vendrá la alegría.

>**Mateo 5:4[^RV60]** Bienaventurados los que lloran, porque ellos recibirán consolación.

> **Isaías 25:8[^RV60]** Destruirá a la muerte para siempre; y enjugará Jehová el Señor toda lágrima de todos los rostros; y quitará la afrenta de su pueblo de toda la tierra; porque Jehová lo ha dicho.

[^RV60]: Biblia Reina Valera 1960