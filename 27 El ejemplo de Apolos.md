# El ejemplo de Apolos

> **Hechos 18:24-26[^RV60]** **24** Llegó entonces a Efeso un judío llamado Apolos, natural de Alejandría, varón elocuente, poderoso en las Escrituras. **25** Este había sido instruido en el camino del Señor; y siendo de espíritu fervoroso, hablaba y enseñaba diligentemente lo concerniente al Señor, aunque solamente conocía el bautismo de Juan. **26** Y comenzó a hablar con denuedo en la sinagoga; pero cuando le oyeron Priscila y Aquila, le tomaron aparte y le expusieron más exactamente el camino de Dios.

Es interesante ver que a alguien como Apolos, según la escritura era alguien elocuente y poderoso en las Escrituras. Además, era de espíritu fervoroso, hablaba y enseñaba diligentemente lo que sabia de Jesucristo.

Pero creo que hay algo que sobresale de todo esto y es que Apolos acepto una corrección, acepto que dos personas desconocidas le explicarán con mayor certeza el camino de Dios.

La humildad de Apolos es la que sobresale, más que su conocimiento y fervor, creo que debemos tomar este ejemplo de Apolos, de que a pesar que pensemos que en algún área somos muy buenos puede venir cualquier persona o situación a exponernos de manera más exacta lo que nosotros conocemos.

En Apolos podemos ver la gran capacidad que podemos llegar a tener pero con cierta carencia, es decir, Apolos sabía con gran precisión lo que le habían enseñado pero solo conocía el bautismo de Juan.

Creo que debemos orar a Dios Padre para que él nos revele de una forma más exacta lo que conocemos de él, no necesariamente que venga alguien más a enseñarnos sino ser humildes para recibir una enseñanza de Dios independientemente del medio que él use, en este caso Dios uso a Priscila y Aquila.

Priscila quiere decir Antiguo(-a) o anciano y Aquila significa águila, si vemos Proverbios 8:22 habla de la antigüedad de la sabiduría la cual estaba con Jehová, y cuando observamos el águila representa algo que está en las alturas lo cual representa a la revelación.

> **Proverbios 8:22[^RV60]** Jehová me poseía en el principio, Ya de antiguo, antes de sus obras.

> **Abdías 1:4[^RV60]** Si te remontares como águila, y aunque entre las estrellas pusieres tu nido, de ahí te derribaré, dice Jehová.

Al ver esto nos damos cuenta que Dios quería que Apolos conociera las cosas como él (Dios) conocía las cosas, por esta causa le envió sabiduría y revelación.

> **Efesios 1:17-18[^RV60]** **17** para que el Dios de nuestro Señor Jesucristo, el Padre de gloria, os dé _espíritu de sabiduría y de revelación_ en el conocimiento de él, **18** alumbrando los ojos de vuestro entendimiento, para que sepáis cuál es la esperanza a que él os ha llamado, y cuáles las riquezas de la gloria de su herencia en los santos,

Seamos como Apolos en la humildad de recibir de parte de Dios un camino más excelente del que conocemos. Y pidamos a Dios un espíritu de sabiduría y revelación como Dios conoce las cosas y no como nosotros queremos conocer las cosas.

> **Isaías 55:8-9[^RV60]** **8** Porque mis pensamientos no son vuestros pensamientos, ni vuestros caminos mis caminos, dijo Jehová. **9** Como son más altos los cielos que la tierra, así son mis caminos más altos que vuestros caminos, y mis pensamientos más que vuestros pensamientos.

[^RV60]: Reina Valera 1960