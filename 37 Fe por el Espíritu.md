# Fe por el Espíritu

> **1 Corintios 12:9** a otro, fe por el mismo Espíritu; y a otro, dones de sanidades por el mismo Espíritu.
 
El que imparte fe a nuestras vidas es el Espíritu Santo. La fe que imparte el Espíritu Santo es creer a la verdad, es decir, creer en Jesucristo para que seamos salvados de una vida falsa y por consecuencia de una condenación.

> **Juan 16:7-9** Pero yo os digo la verdad: Os conviene que yo me vaya; porque si no me fuera, el Consolador no vendría a vosotros; mas si me fuere, os lo enviaré. Y cuando él venga, convencerá al mundo de pecado, de justicia y de juicio. De pecado, por cuanto no creen en mí.

> **Juan 16:13-14** Pero cuando venga el Espíritu de verdad, él os guiará a toda la verdad; porque no hablará por su propia cuenta, sino que hablará todo lo que oyere, y os hará saber las cosas que habrán de venir. El me glorificará; porque tomará de lo mío, y os lo hará saber.

Al creer en la verdad tenemos acceso a la verdad y a la plenitud de la salvación.

Por esto debemos orar para que nos sea impartida fe por el Espíritu Santo para creer a la verdad y no resistir al Espíritu Santo.

> **Hechos 7:51** !!Duros de cerviz, e incircuncisos de corazón y de oídos! Vosotros resistís siempre al Espíritu Santo; como vuestros padres, así también vosotros.

También orar para creer a la verdad, porque sino creemos a la verdad, Dios puede enviarnos un poder engañoso para que creamos a la mentira.

> **2 Tesalonicenses 2:11-12** Por esto Dios les envía un poder engañoso, para que crean la mentira, a fin de que sean condenados todos los que no creyeron a la verdad, sino que se complacieron en la injusticia.

Así que amad@s oremos para que Dios tenga misericordia de nosotros y nos sea impartida la fe por el Espíritu Santo, creamos y amemos la verdad, por que la verdad nos hará libres y en la verdad hay salvación y vida eterna.