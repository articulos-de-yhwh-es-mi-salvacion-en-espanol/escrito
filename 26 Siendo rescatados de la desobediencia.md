# Siendo rescatados de la desobediencia

En el libro de Jonás nos es mostrado como la naturaleza del hombre huye a la obediencia a la palabra, ya que Dios le dio una palabra a Jonás y Jonás lo que hace es huir a la palabra, yendo en una dirección totalmente contraria a la que Dios le había indicado.

> **Jonás 1:1-3[^RV60]** Vino palabra de Jehová a Jonás hijo de Amitai, diciendo: Levántate y ve a Nínive, aquella gran ciudad, y pregona contra ella; porque ha subido su maldad delante de mí. Y Jonás se levantó para huir de la presencia de Jehová a Tarsis, y descendió a Jope, y halló una nave que partía para Tarsis; y pagando su pasaje, entró en ella para irse con ellos a Tarsis, lejos de la presencia de Jehová.

Muchas veces no obedecemos a la palabra que Dios nos da porque no da cabida en nosotros y nosotros por aún tener una vieja naturaleza hace que nos movamos según nuestro propio entendimiento. Por lo que Dios tiene que tratar con nosotros porque nos amá y necesita destruir la vieja naturaleza, una de las cosas que Dios hace es impedir que sigamos por el camino equivocado colocando tribulación.

> **Juan 8:37[^RV60]** Sé que sois descendientes de Abraham; pero procuráis matarme, porque mi palabra no halla cabida en vosotros.

> **Jonás 1:4[^RV60]** Pero Jehová hizo levantar un gran viento en el mar, y hubo en el mar una tempestad tan grande que se pensó que se partiría la nave.

Luego de colocarnos tribulación y ver que no desistimos nos bautiza en la palabra usando a personas que nos lancen hacia la palabra.

> **Habacuc 2:14[^RV60]** Porque la tierra será llena del conocimiento de la gloria de Jehová, como las aguas cubren el mar.

> **Jonás 1:15[^RV60]** Y tomaron a Jonás, y lo echaron al mar, y el mar se aquietó de su furor.

Y luego nos bautiza en fuego para la destrucción de la vieja naturaleza y que podamos clamar a Él y Él nos pueda librar de nuestra vieja naturaleza y desobediencia. ***Jonás 2***

Pero también mientras tanto nos prepará un lugar de refugio para nuestro cuerpo mientras tratan con nuestra alma. 

> **Jonás 1:17[^RV60]** Pero Jehová tenía preparado un gran pez que tragase a Jonás; y estuvo Jonás en el vientre del pez tres día y tres noches.

> **Jonás 2:10[^RV60]** Y mandó Jehová al pez, y vomitó a Jonás en tierra.

Al resucitarnos a una nueva naturaleza cumplimos la palabra la cual nos fue dada al principio y vendrá una manifestación gloriosa de la palabra que Dios nos había dicho.

> **Jonás 3:3[^RV60]** Y se levantó Jonás, y fue a Nínive conforme a la palabra de Jehová. Y era Nínive ciudad grande en extremo, de tres día de camino. Y comenzó Jonás a entrar por la ciudad, camino de un día, y predicaba diciendo: De aquí a cuarenta días Nínive será destruida. Y los hombres de Nínive creyeron a Dios, y proclamaron ayuno, y se vistieron de cilicio desde el mayor hasta el menor de ellos.

Al poner por obra la palabra que Dios nos da, una naturaleza mayor en nuestras vidas como lo es Nínive, figura de una área mayor de nuestra alma que es rebelde y cruel ve que viene su destrucción porque se da cuenta de su maldad y busca la conversión por medio del ayuno, el lloro y el lamento.

> **Joel 2:12-15[^RV60]** Por eso pues, ahora, dice Jehová, convertíos a mí con todo vuestro corazón, con ayuno y lloro y lamento. Rasgad vuestro corazón, y no vuestros vestidos, y convertíos a Jehová vuestro Dios; porque misericordioso es y clemente, tardo para la ira y grande en misericordia y que se arrepentirá y dejará bendición tras de él, esto es, ofrenda y libación para Jehová vuestro Dios? Tocad trompeta en Sion, proclamad ayuno, convocad asamblea.

> **Jonás 3:10[^RV60]** Y vio Dios lo que hicieron, que se conviertieron de su mal camino; y se arrepintió del mal que había dicho que les haría, y no lo hizo.

Gracias sean dadas a Dios que nos recata de nuestra desobediencia y nos guía al arrepentimiento por medio de su palabra.

[^RV60]: Biblia Reina Valera 1960